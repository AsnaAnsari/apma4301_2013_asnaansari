Questions 1, 2a and 2b turned in hard copy.

2c / d:

Forward Euler tfml file: diffusion_theta_zero.tfml

Modified run_convergence.py to produce L1 err.

The timestep is not stable for this problem -- too large

In 2D, maximum stable timestep is given by:

tmax = h^2 / 4

For forward Euler, for h ~ 0.01, would expect the maximum stable timstep to be

tmax = 2.5E-5

L1 error for forward Euler:

N= 100 L1err= 0.0031634762703

L1 error for TRBDF-2 = 

N= 100 L1err= 9.0852045172e-05

Convergence plot for TRBDF2 scheme: trbdf2_Diffusion_convergence.pdf
Convergence plot for forward Euler scheme: FE_Diffusion_convergence.pdf


2e:

Backward Euler tfml file: diffusion_theta_one.tfml

L1error = at t = 0.012, with dt = 0.0002
N= 100 L1err= 0.0030150958657

2f:

Crank-Nicholson scheme: diffusion_theta_CR.tfml

L1 error for Crank-Nicolson: 
N= 100 L1err= 0.0030281230092


I know this isn't right, but I'm not sure why! I can't find any mistakes in my tfml files :(


C-R and Backward Euler are unconditionally stable. Errors not much worse than the second order scheme because they are first order acc. in time and 
second order accurate in space. 

