
#------ PETSc Performance Summary ----------

Nproc = 1
Time = [   7.610e+01,]
Objects = [   4.297e+03,]
Flops = [   1.124e+08,]
Memory = [   0.000e+00,]
MPIMessages = [   0.000e+00,]
MPIMessageLengths = [   2.000e+01,]
MPIReductions = [   8.004e+03,]

#Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
#                       Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
#  0:      Main Stage: 7.6098e+01 100.0%  1.1242e+08 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  6.329e+03  79.1% 

# Event
# ------------------------------------------------------
class Stage(object):
    def __init__(self, name, time, flops, numMessages, messageLength, numReductions):
        # The time and flops represent totals across processes, whereas reductions are only counted once
        self.name          = name
        self.time          = time
        self.flops         = flops
        self.numMessages   = numMessages
        self.messageLength = messageLength
        self.numReductions = numReductions
        self.event         = {}
class Dummy(object):
    pass
Main_Stage = Stage('Main_Stage', 76.0982, 1.12422e+08, 0, 0, 6329)
#
VecMax = Dummy()
Main_Stage.event['VecMax'] = VecMax
VecMax.Count = [       616,]
VecMax.Time  = [   3.584e-02,]
VecMax.Flops = [   0.000e+00,]
VecMax.NumMessages = [   0.0e+00,]
VecMax.MessageLength = [   0.000e+00,]
VecMax.NumReductions = [   0.0e+00,]
#
VecMin = Dummy()
Main_Stage.event['VecMin'] = VecMin
VecMin.Count = [       616,]
VecMin.Time  = [   3.298e-02,]
VecMin.Flops = [   0.000e+00,]
VecMin.NumMessages = [   0.0e+00,]
VecMin.MessageLength = [   0.000e+00,]
VecMin.NumReductions = [   0.0e+00,]
#
VecTDot = Dummy()
Main_Stage.event['VecTDot'] = VecTDot
VecTDot.Count = [        20,]
VecTDot.Time  = [   3.970e-04,]
VecTDot.Flops = [   6.656e+05,]
VecTDot.NumMessages = [   0.0e+00,]
VecTDot.MessageLength = [   0.000e+00,]
VecTDot.NumReductions = [   0.0e+00,]
#
VecNorm = Dummy()
Main_Stage.event['VecNorm'] = VecNorm
VecNorm.Count = [       568,]
VecNorm.Time  = [   2.475e-02,]
VecNorm.Flops = [   1.684e+07,]
VecNorm.NumMessages = [   0.0e+00,]
VecNorm.MessageLength = [   0.000e+00,]
VecNorm.NumReductions = [   0.0e+00,]
#
VecScale = Dummy()
Main_Stage.event['VecScale'] = VecScale
VecScale.Count = [        62,]
VecScale.Time  = [   1.178e-03,]
VecScale.Flops = [   1.032e+06,]
VecScale.NumMessages = [   0.0e+00,]
VecScale.MessageLength = [   0.000e+00,]
VecScale.NumReductions = [   0.0e+00,]
#
VecCopy = Dummy()
Main_Stage.event['VecCopy'] = VecCopy
VecCopy.Count = [       738,]
VecCopy.Time  = [   6.803e-02,]
VecCopy.Flops = [   0.000e+00,]
VecCopy.NumMessages = [   0.0e+00,]
VecCopy.MessageLength = [   0.000e+00,]
VecCopy.NumReductions = [   0.0e+00,]
#
VecSet = Dummy()
Main_Stage.event['VecSet'] = VecSet
VecSet.Count = [      4155,]
VecSet.Time  = [   8.049e-02,]
VecSet.Flops = [   0.000e+00,]
VecSet.NumMessages = [   0.0e+00,]
VecSet.MessageLength = [   0.000e+00,]
VecSet.NumReductions = [   0.0e+00,]
#
VecAXPY = Dummy()
Main_Stage.event['VecAXPY'] = VecAXPY
VecAXPY.Count = [        82,]
VecAXPY.Time  = [   9.568e-03,]
VecAXPY.Flops = [   4.793e+06,]
VecAXPY.NumMessages = [   0.0e+00,]
VecAXPY.MessageLength = [   0.000e+00,]
VecAXPY.NumReductions = [   0.0e+00,]
#
VecAYPX = Dummy()
Main_Stage.event['VecAYPX'] = VecAYPX
VecAYPX.Count = [        71,]
VecAYPX.Time  = [   6.595e-03,]
VecAYPX.Flops = [   2.363e+06,]
VecAYPX.NumMessages = [   0.0e+00,]
VecAYPX.MessageLength = [   0.000e+00,]
VecAYPX.NumReductions = [   0.0e+00,]
#
VecAssemblyBegin = Dummy()
Main_Stage.event['VecAssemblyBegin'] = VecAssemblyBegin
VecAssemblyBegin.Count = [       734,]
VecAssemblyBegin.Time  = [   3.173e-04,]
VecAssemblyBegin.Flops = [   0.000e+00,]
VecAssemblyBegin.NumMessages = [   0.0e+00,]
VecAssemblyBegin.MessageLength = [   0.000e+00,]
VecAssemblyBegin.NumReductions = [   0.0e+00,]
#
VecAssemblyEnd = Dummy()
Main_Stage.event['VecAssemblyEnd'] = VecAssemblyEnd
VecAssemblyEnd.Count = [       734,]
VecAssemblyEnd.Time  = [   9.918e-05,]
VecAssemblyEnd.Flops = [   0.000e+00,]
VecAssemblyEnd.NumMessages = [   0.0e+00,]
VecAssemblyEnd.MessageLength = [   0.000e+00,]
VecAssemblyEnd.NumReductions = [   0.0e+00,]
#
VecScatterBegin = Dummy()
Main_Stage.event['VecScatterBegin'] = VecScatterBegin
VecScatterBegin.Count = [       740,]
VecScatterBegin.Time  = [   9.166e-02,]
VecScatterBegin.Flops = [   0.000e+00,]
VecScatterBegin.NumMessages = [   0.0e+00,]
VecScatterBegin.MessageLength = [   0.000e+00,]
VecScatterBegin.NumReductions = [   0.0e+00,]
#
MatMult = Dummy()
Main_Stage.event['MatMult'] = MatMult
MatMult.Count = [       136,]
MatMult.Time  = [   1.739e-01,]
MatMult.Flops = [   8.673e+07,]
MatMult.NumMessages = [   0.0e+00,]
MatMult.MessageLength = [   0.000e+00,]
MatMult.NumReductions = [   0.0e+00,]
#
MatConvert = Dummy()
Main_Stage.event['MatConvert'] = MatConvert
MatConvert.Count = [       120,]
MatConvert.Time  = [   1.964e-01,]
MatConvert.Flops = [   0.000e+00,]
MatConvert.NumMessages = [   0.0e+00,]
MatConvert.MessageLength = [   0.000e+00,]
MatConvert.NumReductions = [   0.0e+00,]
#
MatAssemblyBegin = Dummy()
Main_Stage.event['MatAssemblyBegin'] = MatAssemblyBegin
MatAssemblyBegin.Count = [       306,]
MatAssemblyBegin.Time  = [   2.663e-04,]
MatAssemblyBegin.Flops = [   0.000e+00,]
MatAssemblyBegin.NumMessages = [   0.0e+00,]
MatAssemblyBegin.MessageLength = [   0.000e+00,]
MatAssemblyBegin.NumReductions = [   0.0e+00,]
#
MatAssemblyEnd = Dummy()
Main_Stage.event['MatAssemblyEnd'] = MatAssemblyEnd
MatAssemblyEnd.Count = [       306,]
MatAssemblyEnd.Time  = [   2.079e-01,]
MatAssemblyEnd.Flops = [   0.000e+00,]
MatAssemblyEnd.NumMessages = [   0.0e+00,]
MatAssemblyEnd.MessageLength = [   0.000e+00,]
MatAssemblyEnd.NumReductions = [   0.0e+00,]
#
MatGetRowIJ = Dummy()
Main_Stage.event['MatGetRowIJ'] = MatGetRowIJ
MatGetRowIJ.Count = [       120,]
MatGetRowIJ.Time  = [   1.025e-04,]
MatGetRowIJ.Flops = [   0.000e+00,]
MatGetRowIJ.NumMessages = [   0.0e+00,]
MatGetRowIJ.MessageLength = [   0.000e+00,]
MatGetRowIJ.NumReductions = [   0.0e+00,]
#
MatGetSubMatrice = Dummy()
Main_Stage.event['MatGetSubMatrice'] = MatGetSubMatrice
MatGetSubMatrice.Count = [       244,]
MatGetSubMatrice.Time  = [   9.242e-01,]
MatGetSubMatrice.Flops = [   0.000e+00,]
MatGetSubMatrice.NumMessages = [   0.0e+00,]
MatGetSubMatrice.MessageLength = [   0.000e+00,]
MatGetSubMatrice.NumReductions = [   1.6e+01,]
#
MatZeroEntries = Dummy()
Main_Stage.event['MatZeroEntries'] = MatZeroEntries
MatZeroEntries.Count = [       182,]
MatZeroEntries.Time  = [   9.037e-02,]
MatZeroEntries.Flops = [   0.000e+00,]
MatZeroEntries.NumMessages = [   0.0e+00,]
MatZeroEntries.MessageLength = [   0.000e+00,]
MatZeroEntries.NumReductions = [   0.0e+00,]
#
SNESSolve = Dummy()
Main_Stage.event['SNESSolve'] = SNESSolve
SNESSolve.Count = [        60,]
SNESSolve.Time  = [   2.017e+01,]
SNESSolve.Flops = [   1.124e+08,]
SNESSolve.NumMessages = [   0.0e+00,]
SNESSolve.MessageLength = [   0.000e+00,]
SNESSolve.NumReductions = [   2.7e+03,]
#
SNESFunctionEval = Dummy()
Main_Stage.event['SNESFunctionEval'] = SNESFunctionEval
SNESFunctionEval.Count = [        60,]
SNESFunctionEval.Time  = [   1.225e+00,]
SNESFunctionEval.Flops = [   0.000e+00,]
SNESFunctionEval.NumMessages = [   0.0e+00,]
SNESFunctionEval.MessageLength = [   0.000e+00,]
SNESFunctionEval.NumReductions = [   0.0e+00,]
#
SNESJacobianEval = Dummy()
Main_Stage.event['SNESJacobianEval'] = SNESJacobianEval
SNESJacobianEval.Count = [        60,]
SNESJacobianEval.Time  = [   4.870e+00,]
SNESJacobianEval.Flops = [   0.000e+00,]
SNESJacobianEval.NumMessages = [   0.0e+00,]
SNESJacobianEval.MessageLength = [   0.000e+00,]
SNESJacobianEval.NumReductions = [   0.0e+00,]
#
KSPSetUp = Dummy()
Main_Stage.event['KSPSetUp'] = KSPSetUp
KSPSetUp.Count = [       180,]
KSPSetUp.Time  = [   1.803e-03,]
KSPSetUp.Flops = [   0.000e+00,]
KSPSetUp.NumMessages = [   0.0e+00,]
KSPSetUp.MessageLength = [   0.000e+00,]
KSPSetUp.NumReductions = [   8.0e+00,]
#
KSPSolve = Dummy()
Main_Stage.event['KSPSolve'] = KSPSolve
KSPSolve.Count = [        60,]
KSPSolve.Time  = [   1.406e+01,]
KSPSolve.Flops = [   1.084e+08,]
KSPSolve.NumMessages = [   0.0e+00,]
KSPSolve.MessageLength = [   0.000e+00,]
KSPSolve.NumReductions = [   2.7e+03,]
#
PCSetUp = Dummy()
Main_Stage.event['PCSetUp'] = PCSetUp
PCSetUp.Count = [       181,]
PCSetUp.Time  = [   1.624e+00,]
PCSetUp.Flops = [   0.000e+00,]
PCSetUp.NumMessages = [   0.0e+00,]
PCSetUp.MessageLength = [   0.000e+00,]
PCSetUp.NumReductions = [   1.6e+02,]
#
PCApply = Dummy()
Main_Stage.event['PCApply'] = PCApply
PCApply.Count = [        62,]
PCApply.Time  = [   1.084e+00,]
PCApply.Flops = [   3.680e+07,]
PCApply.NumMessages = [   0.0e+00,]
PCApply.MessageLength = [   0.000e+00,]
PCApply.NumReductions = [   1.6e+01,]
# ========================================================================================================================
AveragetimetogetPetscTime = 9.53674e-08

