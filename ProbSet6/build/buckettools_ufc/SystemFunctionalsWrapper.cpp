
#include "SystemFunctionalsWrapper.h"
#include "BoostTypes.h"
#include <dolfin.h>
#include "DiffusionTMeanT.h"
#include "DiffusionTL1Err.h"
#include "DiffusionTL1norm.h"
#include "DiffusionTL2ErrSquared.h"
#include "DiffusionThalfMeanT.h"

namespace buckettools
{
  // A function to return a functionspace (for a coefficient) from a system given a mesh, a functionname and a uflsymbol.
  FunctionSpace_ptr ufc_fetch_coefficientspace_from_functional(const std::string &systemname, const std::string &functionname, const std::string &functionalname, const std::string &uflsymbol, Mesh_ptr mesh)
  {
    FunctionSpace_ptr coefficientspace;
    if (systemname ==  "Diffusion")
    {
      if (functionname ==  "T")
      {
        if (functionalname ==  "MeanT")
        {
          dolfin::error("Unknown uflsymbol in ufc_fetch_coefficientspace_from_functional");
        }
        else if (functionalname ==  "L1Err")
        {
          if (uflsymbol ==  "Ta")
          {
            coefficientspace.reset(new DiffusionTL1Err::CoefficientSpace_Ta_i(mesh));
          }
          else
          {
            dolfin::error("Unknown uflsymbol in ufc_fetch_coefficientspace_from_functional");
          }
        }
        else if (functionalname ==  "L1norm")
        {
          dolfin::error("Unknown uflsymbol in ufc_fetch_coefficientspace_from_functional");
        }
        else if (functionalname ==  "L2ErrSquared")
        {
          if (uflsymbol ==  "Ta")
          {
            coefficientspace.reset(new DiffusionTL2ErrSquared::CoefficientSpace_Ta_i(mesh));
          }
          else
          {
            dolfin::error("Unknown uflsymbol in ufc_fetch_coefficientspace_from_functional");
          }
        }
        else
        {
          dolfin::error("Unknown functionalname in ufc_fetch_coefficientspace_from_functional");
        }
      }
      else if (functionname ==  "Thalf")
      {
        if (functionalname ==  "MeanT")
        {
          dolfin::error("Unknown uflsymbol in ufc_fetch_coefficientspace_from_functional");
        }
        else
        {
          dolfin::error("Unknown functionalname in ufc_fetch_coefficientspace_from_functional");
        }
      }
      else if (functionname ==  "AnalyticSolution")
      {
        dolfin::error("Unknown functionalname in ufc_fetch_coefficientspace_from_functional");
      }
      else if (functionname ==  "theta")
      {
        dolfin::error("Unknown functionalname in ufc_fetch_coefficientspace_from_functional");
      }
      else
      {
        dolfin::error("Unknown functionname in ufc_fetch_coefficientspace_from_functional");
      }
    }
    else
    {
      dolfin::error("Unknown systemname in ufc_fetch_coefficientspace_from_functional");
    }
    return coefficientspace;
  }

  // A function to return a functionspace (for a coefficient) from a system given a mesh, a coefficientname and a uflsymbol.
  FunctionSpace_ptr ufc_fetch_coefficientspace_from_functional(const std::string &systemname, const std::string &coefficientname, const std::string &uflsymbol, Mesh_ptr mesh)
  {
    FunctionSpace_ptr coefficientspace;
    if (systemname ==  "Diffusion")
    {
      if (coefficientname ==  "AnalyticSolution")
      {
        dolfin::error("Unknown functional in ufc_fetch_coefficientspace_from_functional");
      }
      else if (coefficientname ==  "theta")
      {
        dolfin::error("Unknown functional in ufc_fetch_coefficientspace_from_functional");
      }
      else
      {
        dolfin::error("Unknown coefficientname in ufc_fetch_coefficientspace_from_functional");
      }
    }
    else
    {
      dolfin::error("Unknown systemname in ufc_fetch_coefficientspace_from_functional");
    }
    return coefficientspace;
  }

  // A function to return a functional from a system-function set given a mesh and a functionalname.
  Form_ptr ufc_fetch_functional(const std::string &systemname, const std::string &functionname, const std::string &functionalname, Mesh_ptr mesh)
  {
    Form_ptr functional;
    if (systemname ==  "Diffusion")
    {
      if (functionname ==  "T")
      {
        if (functionalname ==  "MeanT")
        {
          functional.reset(new DiffusionTMeanT::Form_meanT(mesh));
        }
        else if (functionalname ==  "L1Err")
        {
          functional.reset(new DiffusionTL1Err::Form_L1err(mesh));
        }
        else if (functionalname ==  "L1norm")
        {
          functional.reset(new DiffusionTL1norm::Form_L1norm(mesh));
        }
        else if (functionalname ==  "L2ErrSquared")
        {
          functional.reset(new DiffusionTL2ErrSquared::Form_L2errSquared(mesh));
        }
        else
        {
          dolfin::error("Unknown functionalname in ufc_fetch_functional");
        }
      }
      else if (functionname ==  "Thalf")
      {
        if (functionalname ==  "MeanT")
        {
          functional.reset(new DiffusionThalfMeanT::Form_meanT(mesh));
        }
        else
        {
          dolfin::error("Unknown functionalname in ufc_fetch_functional");
        }
      }
      else if (functionname ==  "AnalyticSolution")
      {
        dolfin::error("Unknown functionalname in ufc_fetch_functional");
      }
      else if (functionname ==  "theta")
      {
        dolfin::error("Unknown functionalname in ufc_fetch_functional");
      }
      else
      {
        dolfin::error("Unknown functionname in ufc_fetch_functional");
      }
    }
    else
    {
      dolfin::error("Unknown systemname in ufc_fetch_functional");
    }
    return functional;
  }

  // A function to return a functional for a constant from a system-function set given a mesh.
  Form_ptr ufc_fetch_functional(const std::string &systemname, const std::string &coefficientname, Mesh_ptr mesh)
  {
    Form_ptr functional;
    if (systemname ==  "Diffusion")
    {
      dolfin::error("Unknown coefficientname in ufc_fetch_functional");
    }
    else
    {
      dolfin::error("Unknown systemname in ufc_fetch_functional");
    }
    return functional;
  }

}

