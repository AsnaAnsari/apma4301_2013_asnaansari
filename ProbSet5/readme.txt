1. The 7 stability plots for problem 1 are in /ProbSet5/plots directory

2. a) Script is called particle_euler, does an example integration with a timestep size of h=0.00001. 
Calculates relative error at each step.

Forward Euler might become unstable for this problem because Taylor expansion of derivative of stream
function around x and y shows that  the stepping scheme characteristic polynomial not bounded by 1 

x(n+1) = x(n)+h*fx(x(n),y(n))
y(n+1) = y(n)+h*fy(x(n),y(n))

Set up rhs such that velocity goes to 0 outside of defined domain

b) Need step size smaller than ~1e-6 (corresponds to n_steps ~ 2E7)

c) Script for RK2 scheme is particle_RK2.py

For an RK2 scheme, need step size smaller than ~1e-5 (corresponds to n_steps ~ 2E6) 

d) Extra Credit: BDF scheme script is called particle_BDF.py

d) Modified file called decaychains_modified.py, produces plot with x and y components of velocity vs time
