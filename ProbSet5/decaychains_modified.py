# -*- coding: utf-8 -*-
"""
Demonstration code for solving stiff systems of equations in scipy

Created on Wed Nov 14 22:27:41 2012

@author: mspieg

Modified by A. Ansari

Using scipy ODE integrator with methods dopri5 and vode with a BDF scheme to solve the general particle tracking problem

dx/dt = (x'(x,y))
dy/dt = (y'(x,y))

"""
from scipy.integrate import ode
from scipy.linalg import expm
from scipy import dot
from numpy import *
import pylab as pl

#set line parameters
pl.rc('lines',linewidth=2)
pl.rc('font',weight='bold')

def particle(t, u):
# RHS to be passed to ODE solver
    return [pi*sin(pi*u[0] / 2.)*cos(pi*u[1]), -1.*(pi/2.)*cos(pi*u[0] / 2.)*sin(pi*u[1])]

def jac(t, u):
    return [[0.5*(pi**2.)*cos(pi*u[0]/2.)*cos(pi*u[1]), -1.*(pi**2)*sin(pi*u[0] / 2.)*sin(pi*u[1])],[0.25*(pi**2)*sin(pi*u[0] / 2.)*sin(pi*u[1]), -0.5*(pi**2.)*cos(pi*u[0] / 2.)*cos(pi*u[1])]]

def stream_func(u):    
    return sin(pi*u[0] / 2.)*sin(pi*u[1])

def runode(ode,u0,t0,tmax,nsteps,showoutput=True):
    """  wrapper function to call ODE for fixed number of steps and return arrays
    """
    t = linspace(t0,tmax,nsteps)
    u = zeros((nsteps,len(u0)))
    ode.set_initial_value(u0,t0)
    # hugely wasteful but evaluate the odeintegrator at points t
    for k in xrange(len(t)):
        ode.set_initial_value(ode.y,ode.t)
        ode.integrate(t[k])
        u[k,:] = ode.y
        if showoutput:
            print t[k], u[k,:]
    return t,u
    
def plotode(t,u,title=None):
    """ utility function for plotting the output of the ODE's
    """
    pl.plot(t,u[:,0], 'r--', t, u[:,1], 'b--')
    pl.xlabel('time')
    pl.ylabel('Particle trajectory')
    if title:
        pl.title(title)
    
u0, t0 = [0.2, 0.25], 0.
tmax = 15.

# calculate exact solution of stream function
u_exact = stream_func(u0)

# set options
RelTol = 1.e-6
AbsTol = 1.e-8

print '\n############# non-stiff solution ###############'
## initialize  the ODE solver using a Dormand Prince 56 pair
ode45 = ode(particle, jac)
ode45.set_integrator('dopri5', atol=AbsTol, rtol=RelTol,nsteps=10000)
t,u = runode(ode45,u0,t0,tmax,10000)
#print 'exact   Non-stiff solution t=',tmax,' x, y position = ',a_exact
print 'dopri5  Non-stiff solution t=',t[-1],' x, y position = ', u, \
  'relative error =',abs(u_exact - stream_func(u))/u_exact
print '################################################\n'

pl.figure()
plotode(t,u,'DormandPrince 56')

# make the problem stiff

t,u = runode(ode45,u0,t0,tmax,100,showoutput=False)
print '\n############# stiff solution ##################'
#print 'exact     stiff solution t=',tmax,' activities = ',a_exact
print 'dopri5    stiff solution t=',t[-1],' x, y coords = ', u, \
  'relative error =',abs(u_exact - stream_func(u))/u_exact
pl.figure()
plotode(t,u,'DormandPrince 56- Stiff')

# now use a proper stiff integrator
odebdf= ode(particle,jac)
odebdf.set_integrator('vode',method='bdf',with_jacobian=True,atol=AbsTol, rtol=RelTol,nsteps=10000)
odebdf.set_initial_value(u0,t0)


t,u = runode(odebdf,u0,t0,tmax,10000)
print 'vode(bdf) stiff solution t=',t[-1],' position = ',u, \
   'relative error =',abs(u_exact - stream_func(u))/u_exact
print '################################################\n'


pl.figure()
plotode(t,u,'bdf scheme')

pl.show(block=True)
