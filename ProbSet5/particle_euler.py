from numpy import *

def x_rhs(x,y):
    """
    return rhs: velocity x component over defined domain [0,2]x[0,1]
    """
    if (0<=x<=2) & (0<=y<=1):
    
        return pi*sin(pi*x / 2.)*cos(pi*y)
    else:
        return 0.
        
def y_rhs(x,y):
    """
    return rhs: y component velocity over restricted domain [0,2]x[0,1]
    """
    if (0<=x<=2) & (0<=y<=1):
        return -1.*(pi/2.)*cos(pi*x / 2.)*sin(pi*y)
    else:
        return 0.
    
def stream_func(x,y):    
    return sin(pi*x / 2.)*sin(pi*y)


def integrate(fx, fy, x0,y0,a,b,h):
    t,x,y = a,x0,y0 # Initial conditions
    stream0 = stream_func(x0,y0) #Initial value of streamfunction
    n_steps = (b-a)/h
    print "number of steps=", n_steps
    print "x_init=",x0
    print "y_init=",y0
    print "x_init=",x
    print "y_init=",y
    
    while t <= b:
        t += h
        print "time=", t
        
        xnew = x + h * fx(x,y)
        ynew = y + h * fy(x,y)
        print "x,y=",xnew,ynew
        rel_err = (stream_func(xnew,ynew)-stream0) / stream0
        print "relative error =", rel_err
        x = xnew
        y = ynew
        
        if abs(rel_err) > 0.001:
            break
        

integrate(x_rhs, y_rhs, 0.2, 0.25, 0., 15., 0.000001) 