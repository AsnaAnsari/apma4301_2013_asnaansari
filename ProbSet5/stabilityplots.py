# -*- coding: utf-8 -*-
"""
Stability diagrams:  draws stability diagrams for various ODE schemes
Created on Tue Nov 13 10:13:08 2012

@author: mspieg

Modified by asna ansari
"""
import numpy as np
import pylab as pl

def plotR(x,y,R,title=None):
    pl.contourf(x,y,np.abs(R),[0.,1.])
    pl.colorbar()
    pl.axis('tight')
    pl.grid()
    if title:
        pl.title(title)
    return
    
bnd=13.    
x=np.linspace(-bnd,bnd,100)
y=np.linspace(-bnd,bnd,100)
X,Y = np.meshgrid(x,y)
Z = X + Y*1j
V=np.array([1.])

# Euler
R = 1.+Z+(Z**2.)/2.

pl.figure()
plotR(x,y,R,'midpoint')
pl.savefig('midpoint2.pdf')
pl.show(False)

