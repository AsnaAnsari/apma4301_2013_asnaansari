\documentclass{amsart}

\usepackage{amssymb, amsmath, centernot, graphicx, courier}
\providecommand{\e}[1]{\ensuremath{\times 10^{#1}}}
 

\title{APMAE4301 Fall 2013 | Thermal Diffusion in Chondrules}
\author{Asna Ansari}
\begin{document}

\maketitle


\section{Problem Outline}

Chondritic meteorites originate from undifferentiated bodies of near-solar composition in the protoplanetary disk. They are formed by the accretion of submillimeter igneous spheres known as chondrules, composed primarily of olivine [(Mg,Fe)$_{2}$SiO$_{4}$], pyroxene [(Mg,Fe,Ca)SiO$_{3}$] and smaller amounts of glass, FeNi metal and troilite (FeS). Chemical and petrologic properties of chondrules point to a widespread and possibly recurrent heating mechanism in the solar nebula, which remains a topic of major contention, as no existing hypothesis is entirely consistent with astronomical and meteoritic evidence. 

\section{Motivation}

Chondrules may form in high-current regions in the turbulent, magnetorotationally unstable flows of the disk. The astrophysical phenomenon of magnetohydrodynamic (MHD) turbulence, a result of magnetorotational instability, creates “current sheets” that may lead to the pervasive, intermittent and localized heating events sufficient to melt chondrule precursors.  \\

As a step toward testing this hypothesis, I propose to study the heating of a dust particle by heat conduction where temperature changes due to the particle's passage through a high temperature region, such as that of a current sheet. I will also explore the thermal behavior of a simplified 2D model of the disk midplane, as a first step toward constraining radial and azimuthal temperature profiles that result from the sudden ``injection'' of heat energy one might expect in an astrophysical current sheet. This study is motivated in part by the dearth of modeling endeavors in the existing literature that attempt to link macroscopic disk behavior with materials properties on microscales. \\ 

My goal is to explore the time-dependent behavior of heat at two dramatically different lengthscales | on the order of $\mu$m for chondrules and ~$10^{9}$ km for disks similar to the progenitor of our own solar system | using finite differencing, and to work through the concomitant dilemmas in optimizing accuracy and minimizing cost while maintaining stability.

\section{Mathematical Model}

Chondrule heating is modeled by solving the heat conduction equation in spherical geometry: 

\begin{align}
\frac{\partial T_C}{\partial t} = \frac{1}{r^2} \frac{\partial}{\partial r}\left(r^2k\frac{\partial T_C}{\partial t}\right) 
\end{align}

where k is the thermal diffusivity coefficient. 
\begin{align}
k = \frac{\lambda}{\rho c_{s}}
\end{align}

$\lambda$ is thermal conductivity, $\rho_{c}$ chondrule density, and $c_{s}$ specific heat. \\


Disk heating is modeled by solving the 2D heat equation with Cartesian geometry with heat source $Q$ (an energy pulse) due to heat production from a current sheet:

\begin{align}
\frac{\partial T_C}{\partial t} = k\nabla^{2} T_{C} + Q = k\Delta T_{C} + Q
\end{align}

Simplifying assumptions and approximations: \\

1. Assume a spherical particle in thermal equilibrium with the surrounding gas. \\

2. Assume particle to be located several optical depths away from any radiation sources in the disk (e.g. the protostar). \\

3. Temperature is homogeneous over the spherical surface, since $v_{part} \ll v_{gas}$ (i.e. ambient gas moves at high velocities relative to dust particles) and the diameter of the chondrule is approximately $1$ mm, orders of magnitude smaller than wavelengths of temperature inhomogeneities. \\

4. Heat production via viscous dissipation occurs predominantly in the disk midplane (1), so the z-component of the disk may be neglected. Solid particles have ``sedimented" to the midplane via gravitational settling, making this the most interesting region for chondrule heating (2). 

\section{Numerical Methods}

I have imposed the initial condition of uniform temperature within the sphere ($0 < r < r_{C}$): 

\begin{align}
T(r,0) = T_{0}
\end{align}

Typical unperturbed temperatures of dust precursors are around 50 K (2). Neumann boundary condition at $r=0$ (insulated center):

\begin{align}
\frac{\partial{T_{c}}}{\partial r}\bigg|_{r=0} = 0
\end{align}

and Dirichlet boundary condition at $r = R_{c}$:

\begin{align}
T_{c}(R_{c}, t) = T_{gas}
\end{align}


\subsection{Choice of discretization}

To solve the spherical problem, I chose an implicit Crank-Nicolson finite difference scheme in time and central differencing in space, which yields a tridiagonal system of equations for the temperature distribution $T(r)$ at different particle radii. The unconditional stability of CN and higher order truncation errors in space and time compared to BTCS and FTCS suggest that it is a judicious choice for this problem. 

I also adopted a constant thermal diffusivity of 1.1\e{-7} $\frac{m^{2}}{s}$, characteristic of chondrules rich in forsteritic olivine.  \\

The discretization is as follows:

\begin{align}
\frac{T_{i}^{n+1}-T_{i}^{n}}{\Delta t} = \frac{k}{2}\left(\frac{T_{i+1}^{n}-2T_{i}^{n}+T_{i-1}^n}{(\Delta r)^2} + \frac{T_{i+1}^{n+1}-2T_{i}^{n+1}+T_{i-1}^{n+1}}{(\Delta r)^2}\right)
\end{align}

which gives a tridiagonal matrix with coefficients of $T_{i}$, $T_{i-1}$ and $T_{i+1}$ on the diagonal, subdiagonal and superdiagonal, respectively. (Note: in 2D the system yields a banded diagonal matrix). \\

An FTCS scheme was used for the 2D problem, with the inclusion of a steady heat source (representative of uniform MRI energy dissipation in the midplane) with Dirichlet boundary conditions $T = 0$ imposed on all sides (this is appropriate, as the grid limits are nearly an order of magnitude larger in all directions than disks of the size that might yield a solar system akin to our own. The boundaries here represent cold interstellar space | effectively 0 K.) \\In the 2D disk model, the dust to gas ratio was assumed to be low, and the thermal diffusivity of a gas of solar composition at $10^{-3}$ bar was taken to be 5\e{-8} $\frac{m^{2}}{s}$. This was a rough order of magnitude approximation derived from existing constraints on density, composition, conductivity (i.e. ionization fraction of the gas), and is likely the largest uncertainty in my formulation of the 2D problem.


\subsection{Algorithm and Software}

Algorithms for solving the above problems numerically were written in Python, taking advantage of the several packages and libraries in writing the solvers, including \texttt{numpy} for data structures, \texttt{scipy} for sparse linear solvers ($O(n)$ direct solution rather than $O(n^3)$ for full matrix).

\subsection{Implementation \& Verification}

Pseudocode:\\1. Set initial condition $T_{o} = 50$ K. \\
2. Set $r$ and $t$ steps with parameter $h = \frac{\Delta t}{\Delta r^{2}}$.\\
3. Impose boundary conditions. \\
4. Build tridiagonal matrix and rhs vector, \texttt{spsolve} for update all inner points at time $t$.\\
5. Step through time, iterate on columns of solution matrix.\\


\section{Results}

Radial temperature profiles for a variety of outer boundary conditions are shown in figures 1 and 2 for a sample diffusivity of 1.1\e{-7} $\frac{m^{2}}{s}$. Scanning the parameter space of diffusivities of chondritic materials reported in the literature resulted in the flow of heat appreciably deeper inner radii, as aspected (see figure 3), and suggest that chondrule compositions are crucial in determining the lower bound ambient temperatures that might result in heating to the liquidus.

Convergence and accuracy of the algorithm were tested using the known solution with Dirichlet B.C.s:

\begin{align}
T(r,t) = \sin(\pi r)e^{exp(-\alpha (\pi^{2}))t}
\end{align}

\begin{align}
T(r,0) = \sin(\pi r)
\end{align}

Results are shown in figures 4 and 5.\\

The 2D FTCS disk model was tested with the Gaussian solution:

\begin{align}
T(x,y,t) = \left(\frac{T_{max}}{1+4t}\right)e^{\frac{x^{2}+y^{2}}{1+4t}}
\end{align}

with the corresponding initial conidition $T(x,y,0)$. Results of convergence tests for FTCS (where the stability constraint $\frac{k\Delta t}{\Delta h^{2}} < 1/4$ was imposed) are shown in Figure 6.  A short movie showing the purported evolution of the disk model is in the \texttt{bitbucket} repo (disktemp.mp4). 


\section{Future Work}

In theory, the calculation presented in this model problem could occur at every node of a complete 3D disk model and as such, minimizing computational cost is as important as achieving convergence. The next step in the process for spherical chondrule heating problems is to examine solver time. While the radial chondrule models appear to give physical results, the 2D FTCS disk model may not be very useful, given its stability constraints.

I might try a $\theta$ scheme or finite volume method next.  In the 2D disk midplane models, matrix equations became expensive to solve | it might make sense to try an Alternating Direction Implicit method (essentially a combination of midpoint and trapezoidal) rather than FTCS, which gave questionable results and had prohibitive stability criterion. (3) 

As dust particle size distributions carry implications for the physics of radiative transfer in the disk, I would like to next experiment with the inclusion of thermal expansion in eq. (1).


\listoffigures


\section{Appendix I: Reflections}

I initially implemented what I thought might be an elegant shortcut to generating the Laplacian operator matrix using the \texttt{networkx} package in Python: the function \texttt{gridgraph} generates an $n$-dimensional grid graph | the discrete Laplacian for $n$ nodes. This was ultimately abandoned for the spherical problem, as it became difficult to use \texttt{scipy}'s sparse solvers in tandem, but it worked well in the disk model for generating the 2D Laplacian on the n$\times$n grid.\\

The first few iterations of the codes utilized a hand-rolled tridiagonal linear solver (\texttt{tridiag.py}), which I quickly realized was just a ponderous, slower (by a little) version of sparse solvers in \texttt{scipy}. I decided to leave it to the professionals. Writing the solver wasn't entirely nugatory: it was a useful pedagogical exercise in understanding the ``under the hood'' machinery of linear solvers.


\begin{figure}[p]
    \centering
    \includegraphics[scale=0.5]{radial}
    \caption{Radial temperature distribution through time.}
    \label{radial}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.5]{chondrule_radialTplot}
    \caption{Solution for exponentially decaying temperature at $r = R_{C}$}
    \label{radial2}
\end{figure}


\begin{figure}[p]
    \centering
    \includegraphics[scale=0.5]{changing_diff}
    \caption{Changing diffusivity to , highest value reported in the literature}
    \label{diff}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.5]{test_acc}
    \caption{Plot of final $T(r)$, close agreement with known solution (blue).}
    \label{accuracy}
\end{figure}


\begin{figure}[p]
    \centering
    \includegraphics[scale=0.5]{convergence1d}
    \caption{Convergence achieved for spherical problem.}
    \label{radialconv}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.5]{convergence2d}
    \caption{Convergence test for 2D FTCS scheme.}
    \label{noop}
\end{figure}



\section{References}

1. Shakura, N. I. and Sunyaev, R. A. (1973). Black Holes in Binary Systems: Observational Appearance, Astronomy and Astrophysics 24, 337–355. \\

2. Connolly, H. C. Jr., \& Hewins, R. H. 1996, in Chondrules and the Protoplanetary Disk, eds. R. H. Hewins, R. H. Jones, \& E. R. D. Scott (Cambridge: Cambridge Univ. Press), 129.\\

3. Peaceman, D., and Rachford, M. (1955). The numerical solution of parabolic and 
elliptic differential equations, J. SIAM 3, 28-41.


\end{document}
