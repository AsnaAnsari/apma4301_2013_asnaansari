import numpy as np

def makegrid(ax,bx,m,gridchoice):
    '''Specify grid points in space for solving a 2-point boundary value problem
       or time-dependent PDE in one space dimension.

       Grid has m interior points on the interval [ax, bx].
       gridchoice specifies the type of grid (see below).
       m+2 grid points (including boundaries) are returned in x.

       Adapted from xgrid.py from http://www.amath.washington.edu/~rjl/fdmbook/  (2007)
    '''

    z = np.linspace(0.0, 1.0, m+2)   # uniform grid in [0,1]

    if gridchoice == 'uniform':
        return ax + (bx-ax)*z

    elif gridchoice == 'rtlayer':
        # Clustered near right boundary
        return ax + (bx-ax) * (1 - (1-z)**2)

    elif gridchoice == 'random':
        # make a sorted array of random values on [0,1)
        s = np.random.random((m+2,))
        s.sort(axis=0)

        # fix endpoints
        x = ax + (bx-ax)*s
        x[0] = ax
        x[m+1] = bx
        return x

    elif gridchoice == 'chebyshev':
        # Chebyshev extreme points
        return ax + (bx-ax) * 0.5*(1 + np.cos(np.pi*(1-z)))

    elif gridchoice == 'legendre':
        # zeros of Legendre polynomial plus endpoints
        Toff = 0.5 / np.sqrt(1-(2.0*np.arange(1,m))**(-2))
        T = np.diag(Toff,1) + np.diag(Toff,-1)
        xi, v = np.linalg.eig(T)
        xi.sort(axis=0)
        s = np.hstack([ [-1], xi, [1] ])
        return ax + (bx-ax) * 0.5*(1 + s)

    else:
        raise Exception("Unknown grid type: %r" % gridchoice)
