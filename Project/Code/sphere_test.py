"""
Diffusion of Heat in Chondrules
APMAE4301: Numerical Methods for PDEs
Fall 2013 Final Project

author: Asna Ansari


Tests the solution to the diffusion eq in 1D with spherical geometry using Crank-Nicolson in t, central differencing in r. 

$\frac{\partial T_C}{\partial t} = \frac{1}{r^2} \frac{\partial}{\partial r}\left(r^2k\frac{\partial T_C}{\partial t}\right)$

Dirichlet boundary conditions, and uniform initial temperature distribution. 

Known solution:

np.sin(np.pi*r)*exp(-alpha*(np.pi**2)*t)
    
where alpha = diffusivity, here 0.1

Produces contour plot with colorbar, showing time evolution of radial temperature profile.

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from networkx import *
import pylab
import makegrid
import CreateMovie as movie
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
from mpl_toolkits.mplot3d import Axes3D


alpha = 0.1

def T_true(radius, time):
    return np.sin(np.pi*radius)*np.exp(-alpha*(np.pi**2)*time)
    
    
## Space discretization parameters
Nr = 100     # Choose the number of interior points in r
rmin = 0.
rmax = 1. # Chondrule radius in mm
h = (rmax - rmin) / float(Nr)     # Compute space step



## Time discretization parameters
k = h / 2. # Adopt timestep equal to half of space step (seconds)
Nt = 200    # Choose the number of time steps
tmin = 0.
tmax = (float(Nt))*k  #Compute tmax in s
K =  0.1 #"canonical" K for avg chondrules is 10e-7 m^2/s = 0.1 mm^2 / s


r = np.linspace(0,rmax,Nr+1)   # grid points r including boundary
t = np.linspace(0,tmax,Nt+1)   # time vector including t_init

# Initialize matrix of solutions. Rows = evolution of a given radius through time. Columns = temperature profile at time t. 
T = np.zeros((Nr+1, Nt+1), float) 


#Initial condition
T[:,0] = np.sin(np.pi*r) # temperature uniform everywhere


# Dirichlet B.C.s
T[0,:] = np.zeros( Nt+1, float )  # Inner
T[Nr,:] = np.zeros( Nt+1, float ) # Uniform outer temperature


# Introduce a simplifying parameter alpha to use in CN scheme: alpha = K / (2*dr^2)

h2 = h*h

# Build matrices for tridiag system

D = (2*k*K*0.5+h2) * np.ones(Nr-1, float )
A = -k * K * 0.5 * np.ones( Nr-1, float )
C = -k * K * 0.5 * np.ones( Nr-1, float )

G = sp.spdiags([A, D, C], [-1,0,1], Nr-1, Nr-1)
G = G.tocsr()

show_matrix = False
if (show_matrix):
    pylab.spy(A,marker='.')

print "Built a sparse coefficient matrix."

  
#Build every new row with the following loop

c = 0.5 # This can be changed if you want to try a theta scheme. 0.5 = Crank-Nicolson

for i in range(0,Nt): 
    # Solve the System: (I - alpha*A) T_new = (I + alpha*A)*T_old => P*T_new = M*T_old
    time = k*i
    rad = (Nr / 2.)*h
    print "time=", time
    
    B = np.zeros( Nr - 1, float )
    B = k * K * ( 1 - c ) * ( T[0:-2,i] + T[2:,i] ) \
                - ( 2 * k * K * ( 1 - c ) - h2 ) * T[1:-1,i]
    B[0] = B[0] + k * K * c * T[0,i+1]
    B[-1] = B[-1] + k * K * c * T[Nr,i+1]
    T[1:-1,i+1] = spsolve(G, B)


#Make a plot of a single time of T_calc and T_true


#print "True solution at tfin=", T_true(r, tmax)

plt.plot(r, T_true(r, tmax), '-', r, T[:,-1], 'r*')
plt.ylabel('Temperature (K)')
plt.xlabel('r (mm)')
plt.title('Temperature profile at tmax for test problem')
plt.legend(('true', 'Crank-Nicolson'))

plt.savefig('test_acc')

