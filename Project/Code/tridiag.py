"""
Diffusion of Heat in Chondrules
APMAE4301 Numerical Methods for PDEs
Fall 2013 Final Project

author: Asna Ansari

Script to solve a tridiagonal matrix equation Ax=b where A is
tridiagonal with main diagonal d, -1 diagonal a, and +1 diagonal c.

This is called in the 1d and 2d PDE solvers

"""

#-----------------------------------------------------------------------------

def factor( a, d, c ):
    """ LU fac of tridiag matrix A
    """

    n = len( d )

    for i in xrange( 1, n ):
        a[i-1] = a[i-1] / d[i-1]
        d[i] = d[i] - a[i-1] * c[i-1]

    return

#-----------------------------------------------------------------------------

def solve( a, d, c, b ):
    """Solves Ax=b for x with factored tridigonal A having diagonals a, d, c. Returns solution vec
    """

    n = len( d )

    x = [0] * n
    x[0] = b[0]

    for i in xrange( 1, n ):
        x[i] = b[i] - a[i-1] * x[i-1]

    x[n-1] = x[n-1] / d[n-1]

    for i in xrange( n-2, -1, -1 ):
        x[i] = ( x[i] - c[i] * x[i+1] ) / d[i]

    return x