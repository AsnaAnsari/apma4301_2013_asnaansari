"""
Diffusion of Heat in Chondrules
APMAE4301: Numerical Methods for PDEs
Fall 2013 Final Project

author: Asna Ansari


Tests the FTCS scheme implemented for the disk model to solve the 2D heat equation w / source term

Known solution:

T(x,y,t)=(1./(1.+4.*t*D))*np.exp(-(x**2.+y**2.)/(1.+4.*t))
    
with initial condition:

T(x,y,t)= exp(-(x**2.+y**2.)

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from networkx import *
import pylab
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve

def T_true(x,y,t):
    """True solution to test algorithm with"""
    return (1./(1.+ 4.*t*D))*np.exp(-(x**2.+y**2.)/(1.+4.*t))


def plot_convergence(N_ar,err_ar):
    """ make pretty convergence plots
    """
    # plot absolute and relative errors against h
    # convert from lists to numpy arrays for plotting
    h = 1./np.array(N_ar)
    err = np.array(err_ar)
    
    # calculate best-fit polynomial to log(h), log(abs_err)
    p = np.polyfit(np.log(h),np.log(err),1)
    pylab.figure()
    pylab.loglog(h,err,'bo-',h,np.exp(p[1])*h**p[0],'k--')
    pylab.xlabel("h")
    pylab.ylabel("error")
    pylab.title("Convergence p={0:3.3}".format(p[0]))
    pylab.legend(["error","best-fit p"],loc="best")
    pylab.grid()
    pylab.savefig('convergence2d')
    
    
    
# A bunch of different grid spacings to try
N_ar=[2, 4, 8, 16]

#N_ar=[2]

#Initialize list of errs
err_ar=[]    

for n in N_ar:    
    
    N = n
    print N

    #plotting params
    xmin = 0.0
    xmax = 1. # AU (astronomical units)
    ymin = 0.0 
    ymax = 1.  # ~1000 AU (astronomical units) 

 
    
    # Thermal diffusivity - calculated roughly from expected composition and densities at low nebular pressures ~10^{-3} bar
    D = 0.25

    # Spatial mesh size
    h = (xmax - xmin) / float(N)
    h2 = float(h**2)

    
    # time step
    dt = 0.1*(h2) / D
    
    tmin = 0
    tmax = 50
    
    # Number of timesteps
    Nt = round((tmax-tmin) / dt) # To evolve to roughly 0.5 Myr, the lifetime of most protoplanetary disks
    Nt = int(Nt)
 
    x, dx = np.linspace(xmin, xmax, N, retstep=True)
    y, dy = np.linspace(ymin, ymax, N, retstep=True)
    t, dt = np.linspace(tmin, tmax, Nt, retstep=True)
 
    #Initialize 3D solution array 
    Tsol = np.zeros((N,N,Nt),float)

    #Set all four boundary edges = 0 (interstellar space outside of the disk)
    #Tsol[:,0,:] =0.
    #Tsol[0,:,:]= 0.
    #Tsol[:,-1,:]= 0.
    #Tsol[-1,:,:]= 0.

# Build initial condition
    for i in range(N):
        for j in range(N):
            x = h*i
            y = h*j
            
            Tsol[i,j,0] = np.exp(-x**2.-y**2.))
    

    # Use simple operator splitting
    for i in range(Nt-1):
        #print i
        Tsol[1:N-1,1:N-1,i+1] = Tsol[1:N-1,1:N-1,i]+(dt*D / h2)*(Tsol[2:N,1:N-1,i]-4.*Tsol[1:N-1,1:N-1,i]+\
        Tsol[0:N-2,1:N-1,i]+Tsol[1:N-1,2:N,i]+Tsol[1:N-1,0:N-2,i]) 
    
    #print Tsol 
        
    #Calculate the error at this value of n
    true = T_true(0.5, 0.5, dt)
    sol = Tsol[N/2,N/2,3]
    err = np.linalg.norm(sol-true)
    print 'Error relative to true solution of PDE = %10.3e \n' % err
    err_ar.append(err)

plot_convergence(N_ar, err_ar)

