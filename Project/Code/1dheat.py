"""
Diffusion of Heat in Chondrules
APMAE4301: Numerical Methods for PDEs
Fall 2013 Final Project

author: Asna Ansari

Solves the diffusion eq in 1D with spherical geometry using Crank-Nicolson in t, central differencing in r. 

$\frac{\partial T_C}{\partial t} = \frac{1}{r^2} \frac{\partial}{\partial r}\left(r^2k\frac{\partial T_C}{\partial t}\right)$

Dirichlet and Neumann boundary conditions, and uniform initial temperature distribution. 

Produces contour plot with colorbar, showing time evolution of radial temperature profile.

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from networkx import *
import pylab
import makegrid
import CreateMovie as movie
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
from mpl_toolkits.mplot3d import Axes3D


## Space discretization parameters
Nr = 100     # Choose the number of interior points in r
rmin = 0.
rmax = 1. # Chondrule radius in mm
h = (rmax - rmin) / float(Nr)     # Compute space step


## Time discretization parameters
k = h / 2. # Adopt timestep equal to half of space step (seconds), as required by stability / accuracy conditions
Nt = 200    # Choose the number of time steps
tmin = 0.
tmax = (float(Nt))*k  #Compute tmax in s
K =  0.1 #"canonical" K for avg chondrules is 10e-7 m^2/s = 0.1 mm^2 / s


r = np.linspace(0,rmax,Nr+1)   # grid points r plus boundaries
#print r.shape
#print "r=", r

t = np.linspace(0,tmax,Nt+1)   # time vector plus initial t

#print t.shape
#print "t=", t


# Initialize matrix of solutions. Rows = evolution of a given radius through time. Columns = temperature profile at time t. 
T = np.zeros((Nr+1, Nt+1), float) 
#print T.shape

#raise SystemExit()


#print "Initialize the solution matrix: \n", T
#print "Initial temp profile: \n", T[0, :]
#print "Evolution of center: \n", T[:,0]

#Initial condition
T[:,0] = 5.*np.zeros(Nr+1) # temperature uniform everywhere


# Dirichlet B.C.s
T[0,:] = np.zeros( Nt+1, float )  # Inner
T[Nr,:] = 60.*np.ones(Nt+1, float) # Uniform outer temperature


# Neumann BC on r = 0. Need to modify the Laplacian for r=0 end-point
#T[0,i+1] = T[0,i] + r/2*(2 u_old(2)-2*u_old(1))


# Introduce a simplifying parameter alpha to use in CN scheme: alpha = K / (2*dr^2)

h2 = h*h

alpha = (K*k) / (2.*(h**2.))

# Build matrices for tridiag system

D = (2*k*K*0.5+h2) * np.ones(Nr-1, float )
A = -k * K * 0.5 * np.ones( Nr-1, float )
C = -k * K * 0.5 * np.ones( Nr-1, float )

G = sp.spdiags([A, D, C], [-1,0,1], Nr-1, Nr-1)
G = G.tocsr()

show_matrix = False
if (show_matrix):
    pylab.spy(G,marker='.')

    
print "Built a sparse coefficient matrix."


#Build every new column in the solution matrix with the following loop

c = 0.5 # This can be changed if you want to try a theta scheme. 0.5 = Crank-Nicolson
for i in range(0,Nt): 
    #print "Starting loop on t"
    
    # Solve the System: (I - alpha*A) T_new = (I + alpha*A)*T_old => P*T_new = M*T_old
    current_time = k*i
    print "time=", current_time
    
    B = np.zeros( Nr - 1, float )
    B = k * K * ( 1 - c ) * ( T[0:-2,i] + T[2:,i] ) \
                - ( 2 * k * K * ( 1 - c ) - h2 ) * T[1:-1,i]
    B[0] = B[0] + k * K * c * T[0,i+1]
    B[-1] = B[-1] + k * K * c * T[Nr,i+1]
    T[1:-1,i+1] = spsolve(G, B)
    
    
    #print T[:,i]
    #print M
    #err[i] = max(abs(T[:,i+1]-Ti))  #Find difference between last two solutions
    #print "error of this iteration=", err[i]
    #if err[i] < errtol:
     #   break # Stop if solutions very similar, have convergence
    
  #  if i==maxiter:
  #      print "Convergence not reached"



"""Make plots"""

#Plot of the entire solution
V, dV = np.linspace(0, 60, 500, retstep=True)

CS = plt.contourf(t,r, T, V)
plt.ylabel('radial distance (mm)')
plt.xlabel('time (s)')
plt.title('Evolving radial temperature distributions for spherical chondrule')


# Make a colorbar for the ContourSet returned by the contourf call.
cbar = plt.colorbar(CS)
cbar.ax.set_ylabel('Temperature (K)')
plt.savefig("chondrule_radialTplot")

#raise SystemExit()

