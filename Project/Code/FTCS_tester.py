"""
Diffusion of Heat in Chondrules
APMAE4301 Numerical Methods for PDEs
Fall 2013 Final Project

author: Asna Ansari

Tests the FTCS scheme implemented for the disk model to solve the 2D heat equation w / source term

Known solution:

T(x,y,t)=(1./(1.+4.*t*D))*np.exp(-(x**2.+y**2.)/(1.+4.*t))
    
with initial condition:

T(x,y,t)= exp(-(x**2.+y**2.)

"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import spdiags
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import pylab
import CreateMovie as movie

 
def T_true(x,y,t):
    """True solution to test algorithm with"""
    D = 1.E-3
    return (1./(1.+4.*t*D))*np.exp(-(x**2.+y**2.)/(1.+4.*t*D))
    
# Number of pts in x and y (always the same)
N = 20

#plotting params
xmin = 0.0
xmax = 1. # AU (astronomical units)
ymin = 0.0 
ymax = 1.  # ~1000 AU (astronomical units) 

 
# Number of timesteps
Nt = 100 # To evolve to roughly 0.5 Myr, the lifetime of most protoplanetary disks

# time step
dt = 0.5

# Thermal diffusivity - calculated roughly from expected composition and densities at low nebular pressures ~10^{-3} bar
D = 1.E-3

# Spatial mesh size
h = (xmax - xmin) / float(N)

h2 = float(h**2)

    
# time step
dt = 0.01*(h2) / D
    
tmin = 0
tmax = 50
    
# Number of timesteps
Nt = round((tmax-tmin) / dt) # To evolve to roughly 0.5 Myr, the lifetime of most protoplanetary disks
Nt = int(Nt)

 
tmin = 0.
tmax = dt*Nt
 
x, dx = np.linspace(xmin, xmax, N, retstep=True)
y, dy = np.linspace(ymin, ymax, N, retstep=True)
t, dt = np.linspace(tmin, tmax, Nt, retstep=True)
 
Y, X = np.meshgrid(y,x)

 
#Initialize 3D solution array 
Tsol = np.zeros((N,N,Nt),float)

#Set all four boundary edges = 0 (interstellar space outside of the disk)

# Build initial condition
for i in range(N):
    for j in range(N):
        x = h*i
        y = h*j
        Tsol[i,j,0] = np.exp(-(x**2.+y**2.))


h2 = float(h**2)

# Use simple operator splitting
for i in range(0,Nt-1):
    for n in range(1,N-1):
        for j in range(1,N-1):
            Tsol[n,j,i+1] = Tsol[n,j,i]+((dt*D) / h2)*(Tsol[n+1,j,i]-2.*Tsol[n,j,i]+\
            Tsol[n-1,j,i]+Tsol[n,j+1,i]-2.*Tsol[n,j,i]+Tsol[n,j-1,i])  

plt.plot(t, T_true(0.5, 0.5, t), t, Tsol[N/2,N/2,:], 'r*')
plt.ylabel('Temperature at the center (K)')
plt.xlabel('t (s)')
plt.title('Tcenter vs t for FTCS test problem')
plt.legend(('true', 'FTCS'))

plt.savefig('test_FTCS')


#raise SystemExit()

