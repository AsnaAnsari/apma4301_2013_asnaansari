Source code for APMA4301 Fall 2013 final project:


1dheat.py: solves time-dependent problem in 1D (sperical geometry) using CN in time, 
centered differences in r with mixed boundary conditions.

1d_test.py verifies that the algorithm from 1dheat.py is behaving as expected using a problem 
formulation with known exact solution, produces a plot of the solutions at same t.

2Dheat.py: same as 1d solver, but in 2D with a heat source (for current sheets in the disk).

disktemp.mp4 is a movie showing the 2D temperature evolution through time of a selected 
time window.

CreateMovie.py and PlayMovie.py are scripts that use ffmpeg to make a movie from several 
plots.