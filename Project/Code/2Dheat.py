"""
Diffusion of Heat in Chondrules
APMAE4301 Numerical Methods for PDEs
Fall 2013 Final Project

author: Asna Ansari

Solves the diffusion eq in 2 dimensions using FTCS with a heat source.

Produces 2D temperature plot of the disk in XY at one "snapshot" in time.

Creates an mp4 file with a movie showing the evolution of the 2D temperature profile

"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import spdiags
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import pylab
import CreateMovie as movie

 
def five_pt_laplacian_sparse(m, a, b):
    """Constructs a sparse matrix that applies the 5-point laplacian discretization"""
    e=np.ones(m**2)
    e2=([1]*(m-1)+[0])*m
    e3=([0]+[1]*(m-1))*m
    h=(b-a)/(m+1)
    A=spdiags([-4*e,e2,e3,e,e],[0,-1,1,-m,m],m**2,m**2)
    A/=h**2
    return A

def C(Tsol):
    """Function for heat source in midplane. Assume current sheet emits energy like a 
    blackbody with emissivity epsilon = 0.5"""
    return 2.

# Number of pts in x and y (always the same)
N = 20
 
#plotting params
xmin = 0.0
xmax = 1. # AU (astronomical units)
ymin = 0.0 
ymax = 1.  # ~1000 AU (astronomical units) 

 
# Number of timesteps
Nt = 100 # To evolve to roughly 0.5 Myr, the lifetime of most protoplanetary disks

# time step
dt = 0.5

# Thermal diffusivity - calculated roughly from expected composition and densities at low nebular pressures ~10^{-3} bar
D = 1.E-2

# Spatial mesh size
h = (xmax - xmin) / float(N)
 
tmin = 0.
tmax = dt*Nt
 
x, dx = np.linspace(xmin, xmax, N, retstep=True)
y, dy = np.linspace(ymin, ymax, N, retstep=True)
t, dt = np.linspace(tmin, tmax, Nt, retstep=True)
 
Y, X = np.meshgrid(y,x)

L = five_pt_laplacian_sparse(N-1, 0.0, 1.0)

 
#Initialize 3D solution array 
Tsol = np.zeros((N,N,Nt+1),float)

#Set all four boundary edges = 0 (interstellar space outside of the disk)
Tsol[:,0,:] =0.
Tsol[0,:,:]= 0.
Tsol[:,-1,:]= 0.
Tsol[-1,:,:]= 0.

# Setting initial temperature: 2D matrix with values in x and y. Everything starts at 10 K.
Tsol[:,:,0] = 10.

h2 = h**2

# Use simple operator splitting
for i in range(0,Nt):
    Tsol[1:N-2,1:N-2,i+1] = Tsol[1:N-2,1:N-2,i]+(dt*D / h2)*(Tsol[2:N-1,1:N-2,i]-2.*Tsol[1:N-2,1:N-2,i]+\
    Tsol[0:N-3,1:N-2,i]+Tsol[1:N-2,2:N-1,i]-2.*Tsol[1:N-2,1:N-2,i]+Tsol[1:N-2,0:N-3,i])+C(Tsol[1:N-2,1:N-2,i])  

    print Tsol[N/2,N/2,5]

V, dV = np.linspace(0, 150, 300, retstep=True)


CS = plt.contourf(x,y,Tsol[:,:,Nt-1], V)
#raise SystemExit()

 
plt.ylabel('distance (AU)')
plt.xlabel('distance (AU)')

# Make a colorbar for the ContourSet returned by the contourf call.
cbar = plt.colorbar(CS)
cbar.ax.set_ylabel('Temperature (K)')
 

plt.savefig("diskplot_2D")

#raise SystemExit()

# Making a movie of the time evolution: plotFunction plots a given snapshot
def plotFunction(frame):
    plt.contourf(x,y,Tsol[:,:,frame], V)
    CS = plt.contourf(x,y,Tsol[:,:,frame], V)
 
    plt.ylabel('distance (AU)')
    plt.xlabel('distance (AU)')

    # Make a colorbar for the ContourSet returned by the contourf call.
    cbar = plt.colorbar(CS)
    cbar.ax.set_ylabel('Temperature (K)')
 
 
movie.CreateMovie(plotFunction, Nt)