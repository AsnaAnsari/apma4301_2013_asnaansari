from sympy import *
from sympy import Derivative as D
from mpmath import *

a = Symbol('a')
b = Symbol('b')
c = Symbol('c')
h = Symbol('h')
x = Symbol('x')
u = Function('u')(x)

# Need to choose one of the three following:

# 1. One-sided approximation for x = 0h on [0h,1h,2h]
#print 'One-sided approximation for x = 0h on [0h,1h,2h]'
#Dux = (a*u)+b*(u+h*D(u,x)+0.5*(h**2)*(D(D(u,x),x)))+(c*(u+2*h*(D(u,x))+0.5*((2*h)**2)*(D(D(u,x),x))))

# 2. Centered approximation for x = h on [0h,1h,2h]
print 'Centered approximation for x = h on [0h,1h,2h]'
Dux = (b*u)+a*(u-h*D(u,x)+0.5*(h**2)*(D(D(u,x),x)))+(c*(u+h*D(u,x)+0.5*(h**2)*(D(D(u,x),x))))

# 3. Two-sided approximation for x = h/2 on [0h,1h,2h]
#print 'Two-sided approximation for x = h/2 on [0h,1h,2h]'
#Dux = (b*(u+(0.5*h)*D(u,x)+0.5*((0.5*h)**2)*(D(D(u,x),x))))+a*(u+(-0.5*h)*D(u,x)+0.5*((-0.5*h)**2)*(D(D(u,x),x)))+(c*(u+1.5*h*D(u,x)+0.5*((1.5*h)**2)*D(D(u,x),x)))

r = collect(expand(Dux),D(u,x), evaluate = False)

#print r[u]
#print r[D(u,x)]
#print r[D(D(u,x))]

# Calculates coefficients for first derivative stencil
s = solve((r[u],r[D(u,x)]-1,r[D(D(u,x))]), a, b, c)
print 'First derivative stencil:'
print '[%s  %s  %s]' %(s[a], s[b], s[c])


#Calculates coefficients for second derivative stencil
q = solve((r[u],r[D(u,x)],r[D(D(u,x))]-1), a, b, c)

print '\n'
print 'Second derivative stencil:'
print '[%s  %s  %s]' %(q[a], q[b], q[c])
