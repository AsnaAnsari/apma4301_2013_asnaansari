from scipy import linalg as LA
from numpy import *
from diffMatrix_modified import setD
import pylab



def get_err():
    N = [8.0,16.0, 32.0, 64.0, 128.0, 256.0, 512.0, 1024.0]
    M = len(N)
    h = empty(M)
    mesh_err1 = empty(M)
    mesh_err2 = empty(M)
    for i in N:
            i = float(i)
            x = linspace(0,1,i+1)
            D1 = setD(1,x)
            D2 = setD(2,x)
            f = x**2+sin(4*pi*x)
            df = 2*x+4*pi*cos(4*pi*x)
            d2f = 2-16*(pi**2)*sin(4*pi*x)	        
# Errors in approximations for first and second derivs
            error1 = D1*f - df
            error2 = D2*f-d2f
            h[N.index(i)] = 1.0/i
            mesh_err1[N.index(i)] = sqrt(1.0/i)*LA.norm(error1, ord = 2)
            mesh_err2[N.index(i)] = sqrt(1.0/i)*LA.norm(error2, ord = 2)
            
    p2 = polyfit(log(h),log(mesh_err1),1)
    p3 = polyfit(log(h),log(mesh_err2),1)
        
    # Plot the mesh errors as a function of h
    
    pylab.figure()
    pylab.loglog(h,mesh_err1,h,mesh_err2, h, exp(p2[1])*h**p2[0], h, exp(p3[1])*h**p3[0])
    print 'Best fit polynomial for first derivative degree = %s' % p2[0]
    print 'Best fit polynomial for second derivative degree = %s' % p3[0]
    
    pylab.legend(['k=1','k=2', 'Degree %s fit'% p2[0], 'Degree %s fit' % p3[0]],loc="best")
    pylab.grid()
    pylab.title("Mesh errors for first and second derivatives")
    pylab.xlabel('h = 1/N')
    pylab.ylabel('Mesh error')
    pylab.show()
   
            
def main():
    get_err()

    

if __name__ == "__main__":
    main()
	
