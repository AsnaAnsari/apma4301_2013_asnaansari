from fdcoeffV import *

print "Coefficients for second derivative at x=0, stencil [0,1,2]"
print fdcoeffV(1,0,[0,1,2])


print "Coefficients for second derivative at x=0, stencil [-2,-1,0,1,2]"
print fdcoeffV(4,0,[-2,-1,0,1,2])
