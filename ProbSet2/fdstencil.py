import numpy as np

from fdcoeffF import fdcoeffF
from factorial import factorial

def fdstencil(k, j):
    '''
    Compute stencil coefficients for finite difference approximation
    of k'th order derivative on a uniform grid.  Print the stencil and
    dominant terms in the truncation error.

    j should be a vector of indices of grid points, values u(x0 + j*h)
    are used, where x0 is an arbitrary grid point and h the mesh spacing.
    This routine returns a vector c of length n=length(j) and the
    k'th derivative is approximated by
      1/h^k * [c(1)*u(x0 + j(1)*h) + ... + c(n)*u(x0 + j(n)*h)].
    Typically j(1) <= 0 <= j(n) and the values in j are monotonically
    increasing, but neither of these conditions is required.
    The routine fdcoeffF is used to compute the coefficients.

    Example:   fdstencil(2, [-1, 0, 1])
       determines the 2nd order centered approximation of the 2nd derivative.

    From  http://www.amath.washington.edu/~rjl/fdmbook/  (2007)
    '''
    
    # make sure j is an array (this allows accepting lists)
    j = np.array(j)

    n = len(j)
    if k >= n:
        raise Exception('length(j) must be larger than k')

    c = fdcoeffF(k, 0, j)      # coefficients for k'th derivative

    # print out stencil:

    print ''
    print 'The derivative u^(%i) of u at x0 is approximated by' % k
    print ''
    print '          1/h^%i * [' % k
    for i in xrange(n-1):
        if j[i] < 0:
            print '                     %22.15e * u(x0%i*h) + ' % (c[i], j[i])
        elif j[i] == 0:
            print '                     %22.15e * u(x0) + ' % c[i]
        else:
            print '                     %22.15e * u(x0+%i*h) + ' % (c[i], j[i])

    print '                     %22.15e * u(x0+%i*h) ]   ' % (c[n-1], j[n-1])


    # determine dominant terms in truncation error and print out:

    err0 = np.dot(c, j**n) / factorial(n)
    err1 = np.dot(c, j**(n+1)) / factorial(n+1)

    # for centered approximations, expect one of err0, err1 to be exactly 0
    if abs(err0) < 1e-14:
        err0 = 0
    if abs(err1) < 1e-14:
        err1 = 0

    print ''
    print 'For smooth u,'
    expvals = (err0, n-k, n, err1, n-k+1, n+1)
    print '\tError = %g * h^%i*u^(%i) + %g * h^%i*u^(%i) + ...' % expvals
    print ''
