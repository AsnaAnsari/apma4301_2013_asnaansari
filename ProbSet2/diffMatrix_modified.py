# -*- coding: utf-8 -*-
"""
diffMatrix:  example program to set up sparse differentiation matrix
Created on Tue Sep 18 01:27:13 2012

@author: mspieg

Modified 9/23 by A. Ansari: this version uses a five-point stencil.
"""

import scipy.sparse as sp
import numpy as np
import pylab

from fdcoeffF import fdcoeffF

def setD(k,x):
    """ 
    example function for setting k'th order sparse differentiation matrix over 
    arbitrary mesh x with a 3 point stencil
    
    input:
        k = degree of derivative <= 2
        x = numpy array of coordinates >=3 in length
    returns:
        D sparse differention matric
    """
    
    assert(k < 3) # check to make sure k < 3
    assert(len(x) > 2)
    
    N = len(x)
    # initialize a sparse NxN matrix in "lil" (linked list) format
    D = sp.lil_matrix((N,N))
    # assign the one-sided k'th derivatives at x[0] and x[1]
    D[0,0:5] = fdcoeffF(k,x[0],x[0:5])
    D[1,0:5] = fdcoeffF(k,x[1],x[0:5])
    # assign centered k'th ordered derivatives in the interior
    for i in xrange(2,N-2):
        D[i,i-2:i+3] = fdcoeffF(k,x[i],x[i-2:i+3])
    # assign one sided k'th derivatives at end points x[-1] and x[-2]
    D[N-2,-5:] = fdcoeffF(k,x[N-2],x[-5:])
    D[N-1,-5:] = fdcoeffF(k,x[N-1],x[-5:])
    
    # convert to csr (compressed row storage) and return
    return D.tocsr()

# quicky test program
def plotDf(x,f,title=None):
    """ Quick test routine to display derivative matrices and plot
        derivatives of an arbitrary function f(x)
        input: x: numpy array of mesh-points
               f: function pointer to function to differentiate
    """  
    # calculate first and second derivative matrices
    D1 = setD(1,x) 
    D2 = setD(2,x)
    
    print D1
    print D2
    
    # show sparsity pattern of D1
    pylab.figure()
    pylab.spy(D1)
    pylab.title("Sparsity pattern of D1")
        
    # plot a function and it's derivatives
    y = f(x)    
    pylab.figure()
    pylab.plot(x,y,x,D1*y,x,D2*y)
    pylab.legend(['f','D1*f','D2*f'],loc="best")
    if title:
        pylab.title(title)
    pylab.show(block=False)


def main():
    # set numpy grid array to be evenly spaced    
    x = np.linspace(0,2,30)
    # choose a simple quadratic function     
    def f(x):
        return x**2 + x
        
    plotDf(x,f,"f=x^2 + x")

    

if __name__ == "__main__":
    main()
