# -*- coding: utf-8 -*-
"""
poisson2d_mms.py:  example program to set up solve poisson equation in 2-D
using manufactured solution u(x) = exp(x+y/2) 

   u_xx+u_yy = exp(x+y/2)+0.25*exp(x+y/2) on x=[0.,1]
   with u= u(x) on the boundaries
   
Created on Tue Sep 18 01:27:13 2012

@author: mspieg
"""

import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import numpy as np
import pylab
from mpl_toolkits.mplot3d import Axes3D

def f_exact(x,y):
    """
    return rhs for manufactured solution given by u_exact
    """
    return 1.25 * np.exp(x+y/2.)
    
def grid_norm2(f,h):
    """calculate grid L2 norm given discrete function f with uniform spacing h
    """
    
    return np.sqrt(h)*np.linalg.norm(f, 2)
    

def plotconvergence(N_ar,err_ar):
    """ make pretty convergence plots
    """
    # plot absolute and relative errors against h
    # convert from lists to numpy arrays for plotting
    h = 1./np.array(N_ar)
    err = np.array(err_ar)
    
    # calculate best-fit polynomial to log(h), log(abs_err)
    p = np.polyfit(np.log(h),np.log(err),1)
    pylab.figure()
    pylab.loglog(h,err,'bo-',h,np.exp(p[1])*h**p[0],'k--')
    pylab.xlabel("h")
    pylab.ylabel("error")
    pylab.title("Convergence p={0:3.3}".format(p[0]))
    pylab.legend(["error","best-fit p"],loc="best")
    pylab.grid()
    pylab.show()
    

def main():
    
    # set number of mesh intervals (mesh points is N+1)
    #N_ar = [16,32,64,128,256]
    N_ar = [8]
    
    # initialize list for storing errors
    err_ar=[]
    for m in N_ar:
        # set numpy grid array to be evenly spaced    
        a = 0.0
        b = 1.0
        h = (b-a)/(m+1)
        x = np.linspace(a,b,m+2)   # grid points x including boundaries
        y = np.linspace(a,b,m+2)   # grid points y including boundaries


        X,Y = np.meshgrid(x,y)     # 2d arrays of x,y values
        X = X.T                    # transpose so that X(i,j),Y(i,j) are
        Y = Y.T                    # coordinates of (i,j) point

        Xint = X[1:-1,1:-1]        # interior points
        Yint = Y[1:-1,1:-1]
        
        utrue = np.exp(X+Y/2.)      # true manufactured solution for test problem

        rhs = f_exact(Xint,Yint)         # evaluate f at interior points for right hand side
                                        # rhs is modified below for boundary conditions.

        # set boundary conditions around edges of usoln array:
        
        usoln = np.copy(utrue)     # use true solution for this test problem
                           # This sets full array, but only boundary values
                           # are used below.  For a problem where utrue
                           # is not known, would have to set each edge of
                           # usoln to the desired Dirichlet boundary values.

        
        # adjust the rhs to include boundary terms: 
        rhs[:,0] -= usoln[1:-1,0] / h**2
        rhs[:,-1] -= usoln[1:-1,-1] / h**2
        rhs[0,:] -= usoln[0,1:-1] / h**2
        rhs[-1,:] -= usoln[-1,1:-1] / h**2


        # convert the 2d grid function rhs into a column vector for rhs of system:
        F = rhs.reshape((m*m,1))
        

        # form matrix A:
        I = sp.eye(m,m)
        e = np.ones(m)
        T = sp.spdiags([e,-4.*e,e],[-1,0,1],m,m)
        S = sp.spdiags([e,e],[-1,1],m,m)
        A = (sp.kron(I,T) + sp.kron(S,I)) / h**2
        A = A.tocsr()

        show_matrix = False
        if (show_matrix):
            pylab.spy(A,marker='.')

    # Solve the linear system:
        uvec = spsolve(A, F)

# reshape vector solution uvec as a grid function and
# insert this interior solution into usoln for plotting purposes:
# (recall boundary conditions in usoln are already set)

        usoln[1:-1, 1:-1] = uvec.reshape( (m,m) )
        
        # calculate error and errornorm
        
        err = np.linalg.norm(usoln-utrue)
        print 'Error relative to true solution of PDE = %10.3e \n' % err

        err_ar.append(err)
        
     
    # plot it out
    plotconvergence(N_ar,err_ar)

    show_result = True
    if show_result:
    # plot results:
        pylab.figure()
        ax = Axes3D(pylab.gcf())
        ax.plot_surface(X,Y,utrue, rstride=1, cstride=1, cmap=pylab.cm.jet)
        ax.set_xlabel('t')
        ax.set_ylabel('x')
        ax.set_zlabel('u')
        #pylab.axis([a, b, a, b])
        #pylab.daspect([1 1 1])
        pylab.title('Surface plot of computed solution')

    pylab.show(block=True)
    

if __name__ == "__main__":
    main()
