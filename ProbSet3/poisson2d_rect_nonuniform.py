# -*- coding: utf-8 -*-
"""
poisson2d_mms_rectangle.py:  example program to set up solve poisson equation in 2-D
using manufactured solution u(x) = exp(x+y/2) on a rectangular domain

   u_xx+u_yy = exp(x+y/2)+0.25*exp(x+y/2) on rectangular domain x=[0.,1], y = [0,2]
   with u= u(x) on the boundaries
   
"""

import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import numpy as np
import pylab
from mpl_toolkits.mplot3d import Axes3D

def u_exact(x,y):
    """ 
    exact solution for this problem
    exp(x+y/2.) 
    """
    return np.exp(x+(y/2.))
    
def f_exact(x,y):
    """
    return rhs for manufactured solution given by u_exact
    """
    return np.exp(x+y/2.)+0.25*np.exp(x+(y/2.))
    
def grid_norm2(f,h):
    """calculate grid L2 norm given discrete function f with uniform spacing h
    """
    return np.sqrt(h)*np.linalg.norm(f, 2)


def main():
    
        # set numpy grid array to be evenly spaced   
        m = 16
        print "Test using {0} grid points in x and y directions".format(m)
        ax = 0.0
        bx = 1.0
        ay = 0.0
        by = 2.0
        print "For rectangular domain [%s,%s] x [%s,%s]" % (ax, bx, ay, by)
        hx = (bx-ax)/(m+1)
        hy= (by-ay)/(m+1)
        hx = float(hx)
        hy = float(hy)
        print hx, hy
        x = np.linspace(ax,bx,m+2)   # grid points x including boundaries
        y = np.linspace(ay,by,m+2)   # grid points y including boundaries


        X,Y = np.meshgrid(x,y)     # 2d arrays of x,y values
        X = X.T                    # transpose so that X(i,j),Y(i,j) are
        Y = Y.T                    # coordinates of (i,j) point

        Xint = X[1:-1,1:-1]        # interior points
        Yint = Y[1:-1,1:-1]
        
        rhs = f_exact(Xint,Yint)         # evaluate f at interior points for right hand side
                                        # rhs is modified below for boundary conditions.

        # set boundary conditions around edges of usoln array:
        usoln = np.zeros(X.shape)     # here we just zero everything  

        usoln[0:-1,0] = u_exact(X[0:-1,0],Y[0,0])
        usoln[0:-1,-1] = u_exact(X[0:-1,-1],Y[-1,-1])
        usoln[0,0:-1]= u_exact(X[0,0],Y[0,0:-1])
        usoln[-1,0:-1]= u_exact(X[-1,0],Y[0,0:-1])
        usoln[-1,-1] = u_exact(X[-1,-1],Y[-1,-1])
        
        
        # adjust the rhs to include boundary terms: 
        rhs[:,0] -= usoln[1:-1,0] / hx**2
        rhs[:,-1] -= usoln[1:-1,-1] / hx**2
        rhs[0,:] -= usoln[0,1:-1] / hy**2
        rhs[-1,:] -= usoln[-1,1:-1] / hy**2


        # convert the 2d grid function rhs into a column vector for rhs of system:
        F = rhs.reshape((m*m,1))

        # form matrix A:
        I = sp.eye(m,m)
        e = np.ones(m)
        T = sp.spdiags([e,-2.*e,e],[-1,0,1],m,m)
        S = sp.spdiags([e,e],[-1,1],m,m)
        K = sp.kron(I,T) / (hx**2)
        L = sp.kron(T,I) / (hy**2)
        A = K+L
        A = A.tocsr()


        show_matrix = False
        if (show_matrix):
            pylab.spy(A,marker='.')

    # Solve the linear system:
        uvec = spsolve(A, F)

# reshape vector solution uvec as a grid function and
# insert this interior solution into usoln for plotting purposes:
# (recall boundary conditions in usoln are already set)

        usoln[1:-1, 1:-1] = uvec.reshape( (m,m) )
        
        # calculate error and errornorm
        umax_true = u_exact(X,Y).max()
        umax = usoln.max()
        
        print umax_true, umax
        abs_err = abs(umax - umax_true)
        rel_err = abs_err/umax_true
        #print "m = {0}".format(m)
        print "||u||_inf = {0}, ||u_true||_inf={1}".format(umax,umax_true)
        print "Absolute error = {0:10.3e}, relative error = {1:10.3e}".format(abs_err,rel_err)

        show_result = True
        if show_result:
            # plot results:
            pylab.figure()
            ax = Axes3D(pylab.gcf())
            ax.plot_surface(X,Y, usoln, rstride=1, cstride=1, cmap=pylab.cm.jet)
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('u')
            pylab.title('Surface plot of computed solution')

            pylab.show(block=True)

if __name__ == "__main__":
    main()
