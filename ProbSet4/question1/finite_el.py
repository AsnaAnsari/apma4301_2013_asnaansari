from sympy import *
from sympy import Derivative as D
from mpmath import *

a = Symbol('a')
b = Symbol('b')
c = Symbol('c')
h = Symbol('h')
x = Symbol('x')
hat_one = 1.-2.*x
hat_twoa = 2.*x
hat_twob = 2.-2.*x
hat_three = 2.*x-1.
f = Function('f')(x)
fp = Function('fp')(x)


f = x*(1-x) / 2.

M11 = nsimplify(integrate(hat_one*hat_one, (x,0.,0.5)))
M12 = nsimplify(integrate(hat_one*hat_twoa, (x,0.,0.5)))
M21 = nsimplify(integrate(hat_one*hat_twoa, (x,0.,0.5)))

M22 = nsimplify(integrate(hat_twoa*hat_twoa, (x,0.,0.5))+integrate(hat_twob*hat_twob, (x,0.5,1.)))
M23 = nsimplify(integrate(hat_twob*hat_three, (x,0.5,1.)))
M32 = nsimplify(integrate(hat_twob*hat_three, (x,0.5,1.)))

M33 = nsimplify(integrate(hat_three*hat_three, (x,0.5,1.)))

print nsimplify(M11), nsimplify(M12), nsimplify(M22), nsimplify(M23), nsimplify(M33)

b1 = integrate(f*hat_one, (x, 0.,0.5))
b2 = integrate(f*hat_twoa, (x, 0.,0.5))+integrate(f*hat_twob,(x,.5,1.))
b3 = integrate(f*hat_three, (x, 0.5,1.))

print expand(f*hat_three)
print integrate(f*hat_three, (x, 0.5,1.))


b = Matrix([[b1,b2,b3]])
print "b="

print nsimplify(b1), nsimplify(b2), nsimplify(b3)

M13 = 0.
M31 = 0.

M = Matrix([[M11,M12,M13],[M21,M22,M23],[M31,M32,M33]])

print M

w = M.inv()*b.T

w0= nsimplify(w[0]) 
w1= nsimplify(w[1])
w2= nsimplify(w[2])

fp_one = hat_one*w0+hat_twoa*w1

fp_two = hat_twob*w1+hat_three*w2

print fp_one, fp_two

error1 = (fp_one-f)**2
print "error1=", error1

error2 = (fp_two-f)**2

print "error2=", error2

normsq = integrate(error1, (x,0.,0.5))+integrate(error2, (x,0.5,1.))

norm = sqrt(normsq)

#print nsimplify(norm)


print "norm=", norm

uh_one = .25*x
uh_two = .25-.25*x

err1 = (uh_one-f)**2
err2 = (uh_two-f)**2

unormsq = integrate(err1, (x,0.,0.5))+integrate(err2, (x,0.5,1.))

print err1
print err2

unorm = sqrt(normsq)

print "unorm=", unorm

print unorm-norm

#print nsimplify(w)





