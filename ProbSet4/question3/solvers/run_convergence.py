# Copyright (C) 2013 Columbia University in the City of New York and others.
#
# Please see the AUTHORS file in the main source directory for a full list
# of contributors.
#
# This file is part of TerraFERMA.
#
# TerraFERMA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TerraFERMA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TerraFERMA. If not, see <http://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Simple python script to run convergence test on the MMS poisson example for TF
Created on Fri Aug  9 17:17:51 2013

@author: mspieg@ldeo.columbia.edu
"""

# import general python modules
import os
import subprocess
from math import sqrt
import pylab as pl
import numpy as np
# import TerraFERMA specific modules: PYTHONPATH needs to be set correctly
import libspud
from buckettools.statfile import parser
from glob import glob

def getNdofs():
    """
    extract number of degrees of freedom from log file assuming SNES_VIEW has been called
    """
    N=[]
    filename = "terraferma.log-0"
    logfile = open(filename,"r")
    for line in logfile:
        if "cols=" in line:
            cols = line.split()[1].replace('cols=','')
            N.append(int(cols))
            
    logfile.close()
    Ndofs = np.unique(np.array(N))  
    return Ndofs
    
def gettiming(logfile):
    """
    loads and reads petsc_summary_python file and extracts total walltime,
    SnesSolve time and Assembly Time
    """
    d = dict()
    execfile(logfile,d)
    Walltime = d['Main_Stage'].time
    Assembly = d['SNESFunctionEval'].Time[0]+d['SNESJacobianEval'].Time[0]
    Solve = d['SNESSolve'].Time[0]
    return Walltime,Solve,Assembly
        
def plotdata(datafile):
    npf = np.load(datafile)
    name = npf['name'].tostring()
    h = 1./npf['N']    
    pl.figure()
    pl.loglog(h,npf['L2err'],'bo-')
    pl.xlabel('h')
    pl.ylabel('relative errors ||e||_2')
    pl.grid()
    p=pl.polyfit(np.log(h),np.log(npf['L2err']),1)
    pl.title('{0} Convergence, p={1}'.format(name,p[0]))
    pl.savefig(name+'_convergence.pdf')

    pl.figure()
    N = npf['N']
    pl.loglog(N,npf['Walltime'],'bo-',N,npf['Solvetime'],'ro-',N,npf['Assembletime'],'go-')
    pl.xlabel('N')
    pl.ylabel('time (s)')
    pl.grid()
    pl.legend(['Wall','Solve','Assemble'],loc='best')
    pl.title(name+' timing')
    pl.savefig(name+'_timing.pdf')

    pl.figure()
    N = npf['N']
    pl.loglog(N,npf['Solvetime']-npf['Assembletime'],'go-')
    pl.xlabel('N')
    pl.ylabel('time (s)')
    pl.grid()
    pl.title(name+' solver - assemble timing')
    pl.savefig(name+'_solver_timing.pdf')

    pl.figure()
    N = npf['N']
    Ndofs = npf['Ndofs']
    Nd = []
    for i in range(Ndofs.shape[0]):
        Nd.append(Ndofs[i][-1])
    Nd = np.array(Nd)
    pl.loglog(N,(npf['Solvetime']-npf['Assembletime'])/Nd,'go-')
    pl.xlabel('N')
    pl.ylabel('time/dof (s)')
    pl.grid()
    pl.title(name+' solve time/dof timing')
    pl.savefig(name+'normalized_solver_timing.pdf')
    
    pl.figure()
    kits = npf['kspits']
    L2res = npf['L2res']
    for i in range(kits.shape[0]):
        pl.semilogy(kits[i],L2res[i])
        pl.hold(True)

    pl.grid()
    pl.xlabel('KSP iterations')
    pl.ylabel('||r||_2')
    pl.title(name+' solver Convergence')
    pl.legend(N,loc='best')
    pl.savefig(name+'_ksp_convergence.pdf')

# run, collect data and plot for a given solver model
def run(name,ncells):
    """ build and run a specific solver model for problems of varying sizes and save the data as numpy arrays"""
    
    #create temporary tfmlfile
    tfmlfile=name+"_convergence.tfml"

    # set python output for petsc_log summary
    summary_file="petscsummary.py"
    os.environ['PETSC_OPTIONS']="-log_summary petsc.log -log_summary_python "+summary_file
   
    # allocate memory for collected data
    L2err = np.zeros(len(ncells))
    Walltime = np.zeros(len(ncells))
    Solvetime = np.zeros(len(ncells))
    Assembletime = np.zeros(len(ncells))
    Ndofs = []
    kspits = []
    L2res = []

    # loop over cells in problem
    for i in xrange(len(ncells)):
        # create temporary tfml file with proper number of cells
        n = ncells[i]
        libspud.clear_options()
        libspud.load_options(name+".tfml")
        libspud.set_option("/geometry/mesh::Mesh/source::UnitSquare/number_cells", [n, n])
        libspud.write_options(tfmlfile)
    
        # build and make program first time
        if i == 0:
            build_dir = 'build_convergence'
            print "Building convergence directory"
            subprocess.call(['tfbuild',tfmlfile,'-d',build_dir])
            os.chdir(build_dir)
            print 'Compiling program'
            subprocess.call(['make','-j','2'])
            os.chdir('..')
        
        os.chdir(build_dir)

        # run the program for convergence info
        #print "running program convergence file"
        prog = './'+name+'_convergence'
        subprocess.call([prog,'../'+tfmlfile,'-l','-vINFO'])
        #extract the L2error from the statfile    
        ioname = libspud.get_option("/io/output_base_name")
        stats =  parser(ioname+".stat")
        L2errorSquared = stats['Poisson']['u']['L2NormErrorSquared'][-1]
        L2Squared =  stats['Poisson']['u']['L2NormSquared'][-1]
        L2err[i] = sqrt(abs(L2errorSquared))/sqrt(L2Squared)
        # extract the convergence file
        convfile = glob(ioname+'*.conv')[0]
        conv = parser(convfile)
        kspits.append(conv['KSPIteration']['value'])
        L2res.append(conv['Poisson']['res_norm(l2)'])
        #extract the number of degrees of freedom from the log file ---total hack
        N= getNdofs()
        Ndofs.append(N)
    
        # run the progam for timing
        if libspud.have_option("/system::Poisson/nonlinear_solver[0]/type::SNES/linear_solver/iterative_method[0]/monitors/convergence_file"):
            libspud.delete_option("/system::Poisson/nonlinear_solver[0]/type::SNES/linear_solver/iterative_method[0]/monitors/convergence_file")
            libspud.write_options('../'+tfmlfile)

        #print "running program for timing"
        subprocess.call([prog,'../'+tfmlfile,'-l','-vINFO'])

        # get timing info
        Walltime[i],Solvetime[i],Assembletime[i] = gettiming(summary_file)
    
        print 'N=', n, \
         'L2err=', L2err[i],'L2res=',L2res[i][-1], \
         'KSPits=',kspits[i][-1], \
         ' Wall=',Walltime[i],' Solve=',Solvetime[i],' Assemble=',Assembletime[i], \
         ' s Ndofs=',Ndofs[i] \

        os.chdir('..')

    # save the output data
    datafile = name+'_convergence_data.npz'
    np.savez(datafile,name=name,N=np.array(ncells),L2err=L2err,L2res=np.array(L2res),
             kspits=np.array(kspits),Walltime=Walltime,Solvetime=Solvetime,Assembletime=Assembletime,Ndofs=np.array(Ndofs))
    return datafile

def main(name,ncells):
    """ run models, collect data and make initial plots for given solver pairs"""
    datafile = run(name, ncells);
    plotdata(datafile)
    pl.show()

if __name__ == "__main__":
    name = 'poisson_richardson,sor'
    ncells = [ 16, 32,64, 128 ]
    main(name,ncells)
