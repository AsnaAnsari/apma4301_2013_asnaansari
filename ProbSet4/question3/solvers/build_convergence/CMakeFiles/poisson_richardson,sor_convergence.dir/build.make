# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /opt/local/bin/cmake

# The command to remove a file.
RM = /opt/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /opt/local/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/Asna/Work/TerraFERMA/share/terraferma/cpp

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/Asna/apma4301_2013_asnaansari/ProbSet4/question3/solvers/build_convergence

# Include any dependencies generated for this target.
include CMakeFiles/poisson_richardson,sor_convergence.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/poisson_richardson,sor_convergence.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/poisson_richardson,sor_convergence.dir/flags.make

CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o: CMakeFiles/poisson_richardson,sor_convergence.dir/flags.make
CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o: /Users/Asna/Work/TerraFERMA/share/terraferma/cpp/main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /Users/Asna/apma4301_2013_asnaansari/ProbSet4/question3/solvers/build_convergence/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o -c /Users/Asna/Work/TerraFERMA/share/terraferma/cpp/main.cpp

CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /Users/Asna/Work/TerraFERMA/share/terraferma/cpp/main.cpp > CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.i

CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /Users/Asna/Work/TerraFERMA/share/terraferma/cpp/main.cpp -o CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.s

CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o.requires:
.PHONY : CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o.requires

CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o.provides: CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o.requires
	$(MAKE) -f CMakeFiles/poisson_richardson,sor_convergence.dir/build.make CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o.provides.build
.PHONY : CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o.provides

CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o.provides.build: CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o

# Object files for target poisson_richardson,sor_convergence
poisson_richardson,sor_convergence_OBJECTS = \
"CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o"

# External object files for target poisson_richardson,sor_convergence
poisson_richardson,sor_convergence_EXTERNAL_OBJECTS =

poisson_richardson,sor_convergence: CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o
poisson_richardson,sor_convergence: CMakeFiles/poisson_richardson,sor_convergence.dir/build.make
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libdolfin.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libxml2.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libarmadillo.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_filesystem.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_program_options.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_system.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_thread.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_iostreams.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_mpi.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_serialization.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_timer.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_chrono.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/src/petsc/tferma-darwin11.1.0-cxx-opt/lib/libpetsc.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libumfpack.a
poisson_richardson,sor_convergence: /opt/local/lib/libamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcholmod.a
poisson_richardson,sor_convergence: /opt/local/lib/libamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcolamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libccolamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libsuitesparseconfig.a
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libsuitesparseconfig.a
poisson_richardson,sor_convergence: /opt/local/lib/libcholmod.a
poisson_richardson,sor_convergence: /opt/local/lib/libamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcolamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libccolamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libsuitesparseconfig.a
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libCGAL.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_thread.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_system.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libgmp.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libmpfr.dylib
poisson_richardson,sor_convergence: /usr/lib/libz.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libcppunit.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmpi_cxx.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmpi.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libopen-rte.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libopen-pal.dylib
poisson_richardson,sor_convergence: /usr/lib/libutil.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkCommon.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkFiltering.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkImaging.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkGraphics.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkGenericFiltering.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkIO.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkRendering.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkVolumeRendering.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkHybrid.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkWidgets.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkInfovis.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkGeovis.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkViews.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkCharts.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libbuckettools_cpp.dylib
poisson_richardson,sor_convergence: buckettools_ufc/libbuckettools_ufc.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libspud.a
poisson_richardson,sor_convergence: /opt/local/lib/libpython2.7.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libdolfin.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libxml2.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libarmadillo.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_filesystem.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_program_options.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_system.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_thread.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_iostreams.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_mpi.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_serialization.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_timer.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_chrono.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/src/petsc/tferma-darwin11.1.0-cxx-opt/lib/libpetsc.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libumfpack.a
poisson_richardson,sor_convergence: /opt/local/lib/libamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcholmod.a
poisson_richardson,sor_convergence: /opt/local/lib/libcamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcolamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libccolamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libsuitesparseconfig.a
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libCGAL.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_filesystem.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_program_options.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_system.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_thread.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_iostreams.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_mpi.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_serialization.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_timer.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libboost_chrono.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/src/petsc/tferma-darwin11.1.0-cxx-opt/lib/libpetsc.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libumfpack.a
poisson_richardson,sor_convergence: /opt/local/lib/libamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcholmod.a
poisson_richardson,sor_convergence: /opt/local/lib/libcamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libcolamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libccolamd.a
poisson_richardson,sor_convergence: /opt/local/lib/libsuitesparseconfig.a
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libCGAL.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libgmp.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libmpfr.dylib
poisson_richardson,sor_convergence: /usr/lib/libz.dylib
poisson_richardson,sor_convergence: /opt/local/lib/libcppunit.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmpi_cxx.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libmpi.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libopen-rte.dylib
poisson_richardson,sor_convergence: /Users/Asna/Work/TerraFERMA/lib/libopen-pal.dylib
poisson_richardson,sor_convergence: /usr/lib/libutil.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkCommon.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkFiltering.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkImaging.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkGraphics.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkGenericFiltering.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkIO.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkRendering.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkVolumeRendering.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkHybrid.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkWidgets.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkInfovis.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkGeovis.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkViews.dylib
poisson_richardson,sor_convergence: /opt/local/lib/vtk-5.10/libvtkCharts.dylib
poisson_richardson,sor_convergence: CMakeFiles/poisson_richardson,sor_convergence.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable poisson_richardson,sor_convergence"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/poisson_richardson,sor_convergence.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/poisson_richardson,sor_convergence.dir/build: poisson_richardson,sor_convergence
.PHONY : CMakeFiles/poisson_richardson,sor_convergence.dir/build

CMakeFiles/poisson_richardson,sor_convergence.dir/requires: CMakeFiles/poisson_richardson,sor_convergence.dir/main.cpp.o.requires
.PHONY : CMakeFiles/poisson_richardson,sor_convergence.dir/requires

CMakeFiles/poisson_richardson,sor_convergence.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/poisson_richardson,sor_convergence.dir/cmake_clean.cmake
.PHONY : CMakeFiles/poisson_richardson,sor_convergence.dir/clean

CMakeFiles/poisson_richardson,sor_convergence.dir/depend:
	cd /Users/Asna/apma4301_2013_asnaansari/ProbSet4/question3/solvers/build_convergence && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/Asna/Work/TerraFERMA/share/terraferma/cpp /Users/Asna/Work/TerraFERMA/share/terraferma/cpp /Users/Asna/apma4301_2013_asnaansari/ProbSet4/question3/solvers/build_convergence /Users/Asna/apma4301_2013_asnaansari/ProbSet4/question3/solvers/build_convergence /Users/Asna/apma4301_2013_asnaansari/ProbSet4/question3/solvers/build_convergence/CMakeFiles/poisson_richardson,sor_convergence.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/poisson_richardson,sor_convergence.dir/depend

