# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Asna/Work/TerraFERMA/share/terraferma/cpp/main.cpp" "/Users/Asna/apma4301_2013_asnaansari/ProbSet4/question3/solvers/build_convergence/CMakeFiles/poisson_cg,none_convergence.dir/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BOOST_UBLAS_NDEBUG"
  "CGAL_DISABLE_ROUNDING_MATH_CHECK"
  "DOLFIN_VERSION=\"1.2.0+\""
  "HAS_CGAL"
  "HAS_CHOLMOD"
  "HAS_CPPUNIT"
  "HAS_MPI"
  "HAS_OPENMP"
  "HAS_PARMETIS"
  "HAS_PETSC"
  "HAS_UMFPACK"
  "HAS_VTK"
  "HAS_ZLIB"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/Asna/apma4301_2013_asnaansari/ProbSet4/question3/solvers/build_convergence/buckettools_ufc/CMakeFiles/buckettools_ufc.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/Asna/Work/TerraFERMA/include"
  "/opt/local/include/libxml2"
  "/opt/local/include"
  "/Users/Asna/Work/TerraFERMA/src/petsc/include"
  "/Users/Asna/Work/TerraFERMA/src/petsc/tferma-darwin11.1.0-cxx-opt/include"
  "/opt/local/include/vtk-5.10"
  "/opt/local/Library/Frameworks/Python.framework/Headers"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
