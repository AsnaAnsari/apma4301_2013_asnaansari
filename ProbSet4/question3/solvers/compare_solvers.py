# Copyright (C) 2013 Columbia University in the City of New York and others.
#
# Please see the AUTHORS file in the main source directory for a full list
# of contributors.
#
# This file is part of TerraFERMA.
#
# TerraFERMA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TerraFERMA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TerraFERMA. If not, see <http://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Simple python script to compare solvers ofr the  MMS poisson example for TF
Created on Fri Aug  9 17:17:51 2013

@author: mspieg@ldeo.columbia.edu
"""

# import general python modules

import pylab as pl
import numpy as np
# import TerraFERMA specific modules: PYTHONPATH needs to be set correctly
from buckettools.statfile import parser
from run_convergence import *
from glob import glob

def getname(root,solver):
    """ construct name of tfml file from root and solver tuple"""
    return root+'_'+solver[0]+','+solver[1]

def comparetime(root,solvers,datafiles):
    """plots runtime/dof for various solvers"""
    pl.figure()
    legend = []    
    for i in range(len(datafiles)):
        legend.append('('+datafiles[i].split('_')[1]+')')
        npf = np.load(datafiles[i])
        N = npf['N']
        Ndofs = npf['Ndofs']
        Nd = []
        for i in range(Ndofs.shape[0]):
            Nd.append(Ndofs[i][-1])
        Nd = np.array(Nd)
        pl.loglog(N,(npf['Solvetime']-npf['Assembletime'])/Nd,'-o')
        pl.hold(True)
        
    pl.xlabel('N')
    pl.ylabel('time/dof (s)')
    pl.grid()
    pl.title(root+' solve time/dof timing')
    pl.legend(legend,loc='best')
    pl.savefig(root+'_compare_normalized_solver_timing.pdf')
    
def comparekspconvergence(root,solvers,datafiles,Nplot):
    """plots  convergence behavior of solvers for largest runs """
    pl.figure()
    legend = []    
    for i in range(len(datafiles)):
        legend.append('('+datafiles[i].split('_')[1]+')')
        npf = np.load(datafiles[i])
        N = npf['N']
        if Nplot in N:
            j = np.where(N==Nplot)[0]
        else:
            print 'problem size ', Nplot, 'not available'
        
        kits = npf['kspits'][j][0]
        L2res = npf['L2res'][j][0]
        pl.semilogy(kits,L2res)
        pl.hold(True)   

    pl.grid()
    pl.xlabel('KSP iterations')
    pl.ylabel('||r||_2')
    pl.title(' {0} solver Convergence for N={1}'.format(root,Nplot))
    pl.legend(legend,loc='best')
    pl.savefig(root+'_ksp_convergence.pdf')
   
def compareconvergence(root,solvers,datafiles):
    """plots  convergence behavior of truncation error for various solvers"""
    pl.figure()
    legend = []    
    for i in range(len(datafiles)):
        npf = np.load(datafiles[i])
        h = 1./npf['N']
        pl.loglog(h,npf['L2err'],'o-')
        p=pl.polyfit(np.log(h),np.log(npf['L2err']),1)
        solver_str = '('+datafiles[i].split('_')[1]+')'
        legend.append(solver_str+' p={0}'.format(p[0]))
        pl.hold(True)   

    pl.grid()
    pl.xlabel('h')
    pl.ylabel('||e||_2')
    pl.title(' {0} Truncation Error Convergence'.format(root))
    pl.legend(legend,loc='best')
    pl.savefig(root+'_mms_convergence.pdf')
   
    
# number of cells for each problem
root = 'poisson'
solvers = [('richardson','lu'),('richardson','lu_mumps'),('richardson','sor'),('gmres','none'),('cg','ilu'),('gmres','ilu'),('cg','ml'),('cg','none')]
ncells = [ 16,32,64,128]
datafiles = []


# loop over solvers
for solver in solvers:
    name = getname(root,solver)
    datafile = glob(name+'*.npz')
    if not datafile:
        datafile = run(name,ncells)
    else:
        datafile = datafile[0]
    print datafile
    #plotdata(datafile)    
    datafiles.append(datafile)

comparetime(root,solvers,datafiles)
comparekspconvergence(root,solvers,datafiles,ncells[-1])
compareconvergence(root,solvers,datafiles)
