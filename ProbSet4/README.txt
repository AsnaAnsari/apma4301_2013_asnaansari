----------------
Question 1
----------------

Answers in hard copy, sympy script in ProbSet4/question1/finite_el.py

-----------------
Question 2a:
-----------------
poisson.tfml file made from scratch is in /ProbSet4/question2/mypoisson

Paraview image of solution is: ProbSet4/question2/2a_screenshot.png

Comments on TF usability: Manual is very clear, a complete inventory / index of all options and a phrase describing what they do might be useful (something like a help tab maybe)

------------------
Question 2b (i):
------------------

Modified tfml file: /ProbSet4/question2/mms/poisson.tfml

Changes made: in tfml file changed source from UnitSquare to Rectangle. Set locations of lower_left and upper_right to correspond to rectangular domain [-1,1]x[0,1]

In run_convergence.py changed libspud.set_option("/geometry/mesh::Mesh/source::UnitSquare/number_cells", [n, n]) to
libspud.set_option("/geometry/mesh::Mesh/source::Rectangle/number_cells", [n, n])
    
Convergence plot: /ProbSet4/question2/mms/poisson_convergence_rect.pdf

No, changing the domain does not change convergence p~2 (still converges as h^2)

------------------
Question 2b (ii):
------------------

In tfml file changed "element" to P2 (under field), change io -> visualization -> element to P2.

In system(Poisson), changed element to P2 under field (u), and changed type (Expression) to P2 for coefficient(f),coefficient(g),coefficient(AnalyticSolution)

P2 Convergence plot: ProbSet4/question2/mmsP2/poisson_convergence_P2.pdf

The order of convergence is p = 3

--For the P1 element solution, you need or N ~300 for get error < 1e-6. For the P2 element solution you only need N > ~20 (estimation from convergence plot)
 
--Degrees of freedom = [number of nodes] x [number of values of the field variable]
P1 3 x N = ~1000
P2 6 x N = ~120

Since dofs scale with N^2, need roughly 1e6 for P1, 400 for P2

--Wall-time: plots ProbSet4/question2/mmsP2/poisson_walltime_P2.pdf and ProbSet4/question2/mmsP2/poisson_walltime_P1.pdf
it's faster (i.e. takes less wall time) to increase p than it is to refine h

------------------
Question 2b (iii):
------------------
Used gmsh to make a domain, used the following commands to make an unstructured 2D mesh:

$ gmsh −2 −algo del2d widget.geo
$ dolfin−convert widget.geo widget.xml

(I wasn't able to create a working .xml file on my machine because of some kind of dolfin installation problem I wasn't able to figure out (it didn't recognize the .geo file),
but it is a functional .geo file that creates a mesh on any other machine, and runs with the tfml file I made).

The geometry file is called widget.geo, and is located in ProbSet4/question2/gmsh/mesh
This file defines 9 "Physical Lines", all of which are set to equal the function in the .tfml file (Dirichlet conditions on all boundaries)

Changed the poisson.tfml file to set Dirichlet boundary conditions everywhere

In form(Residual) under nonlinear solver entered F = (inner(grad(u_t), grad(u_i)) - u_t*f)*dx 
to define the weak form of the residual given on field u and coefficient f.

------------------
Question 3:
------------------
I ran my solver experiments on the Poisson rectangle problem from 2b(i)

Plots are:

ProbSet4/question3/solvers/newpoisson_compare_normalized_solver_timing.pdf
ProbSet4/question3/solvers/newpoisson_ksp_convergence.pdf
ProbSet4/question3/solvers/newpoisson_mms_convergence.pdf

For increasing problem size, GMRES with and without preconditioners begins to take a long time / many iterations. Richardson with SOR preconditioner takes much too long
CG appears to be the fastest solver, consistently, for larger problems. 
I would recommend cg (converge fastest, even for larger problems
in the fewest number of iterations)

Rel. error: set several orders of magnitude below known truncation error for CG, even lower than this for Richardson.
For GMRES, set to at least 1e-16
For GMRES, and Richardson need to allow many more iterations

