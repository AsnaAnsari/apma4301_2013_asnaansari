# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /opt/local/bin/cmake

# The command to remove a file.
RM = /opt/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /opt/local/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/Asna/Work/TerraFERMA/share/terraferma/cpp

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence

# Include any dependencies generated for this target.
include buckettools_ufc/CMakeFiles/buckettools_ufc.dir/depend.make

# Include the progress variables for this target.
include buckettools_ufc/CMakeFiles/buckettools_ufc.dir/progress.make

# Include the compile flags for this target's objects.
include buckettools_ufc/CMakeFiles/buckettools_ufc.dir/flags.make

buckettools_ufc/SystemFunctionalsWrapper.cpp: /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/poisson_convergence.tfml
buckettools_ufc/SystemFunctionalsWrapper.cpp: /Users/Asna/Work/TerraFERMA/bin/systemwrappers_from_options
	$(CMAKE_COMMAND) -E cmake_progress_report /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold "Generating SystemFunctionalsWrapper.cpp, SystemSolversWrapper.cpp, SystemExpressionsWrapper.cpp, VisualizationWrapper.cpp"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /Users/Asna/Work/TerraFERMA/bin/systemwrappers_from_options /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/poisson_convergence.tfml

buckettools_ufc/SystemSolversWrapper.cpp: buckettools_ufc/SystemFunctionalsWrapper.cpp

buckettools_ufc/SystemExpressionsWrapper.cpp: buckettools_ufc/SystemFunctionalsWrapper.cpp

buckettools_ufc/VisualizationWrapper.cpp: buckettools_ufc/SystemFunctionalsWrapper.cpp

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/flags.make
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o: buckettools_ufc/SystemFunctionalsWrapper.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o -c /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemFunctionalsWrapper.cpp

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.i"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemFunctionalsWrapper.cpp > CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.i

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.s"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemFunctionalsWrapper.cpp -o CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.s

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o.requires:
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o.requires

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o.provides: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o.requires
	$(MAKE) -f buckettools_ufc/CMakeFiles/buckettools_ufc.dir/build.make buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o.provides.build
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o.provides

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o.provides.build: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/flags.make
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o: buckettools_ufc/SystemSolversWrapper.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o -c /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemSolversWrapper.cpp

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.i"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemSolversWrapper.cpp > CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.i

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.s"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemSolversWrapper.cpp -o CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.s

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o.requires:
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o.requires

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o.provides: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o.requires
	$(MAKE) -f buckettools_ufc/CMakeFiles/buckettools_ufc.dir/build.make buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o.provides.build
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o.provides

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o.provides.build: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/flags.make
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o: buckettools_ufc/SystemExpressionsWrapper.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o -c /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemExpressionsWrapper.cpp

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.i"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemExpressionsWrapper.cpp > CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.i

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.s"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/SystemExpressionsWrapper.cpp -o CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.s

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o.requires:
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o.requires

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o.provides: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o.requires
	$(MAKE) -f buckettools_ufc/CMakeFiles/buckettools_ufc.dir/build.make buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o.provides.build
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o.provides

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o.provides.build: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/flags.make
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o: buckettools_ufc/VisualizationWrapper.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/CMakeFiles $(CMAKE_PROGRESS_5)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o -c /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/VisualizationWrapper.cpp

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.i"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/VisualizationWrapper.cpp > CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.i

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.s"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/VisualizationWrapper.cpp -o CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.s

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o.requires:
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o.requires

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o.provides: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o.requires
	$(MAKE) -f buckettools_ufc/CMakeFiles/buckettools_ufc.dir/build.make buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o.provides.build
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o.provides

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o.provides.build: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o

# Object files for target buckettools_ufc
buckettools_ufc_OBJECTS = \
"CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o" \
"CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o" \
"CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o" \
"CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o"

# External object files for target buckettools_ufc
buckettools_ufc_EXTERNAL_OBJECTS =

buckettools_ufc/libbuckettools_ufc.dylib: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o
buckettools_ufc/libbuckettools_ufc.dylib: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o
buckettools_ufc/libbuckettools_ufc.dylib: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o
buckettools_ufc/libbuckettools_ufc.dylib: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o
buckettools_ufc/libbuckettools_ufc.dylib: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/build.make
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libdolfin.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libxml2.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libarmadillo.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_filesystem.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_program_options.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_system.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_thread.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_iostreams.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_mpi.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_serialization.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_timer.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_chrono.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/src/petsc/tferma-darwin11.1.0-cxx-opt/lib/libpetsc.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libumfpack.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcholmod.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcolamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libccolamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libsuitesparseconfig.a
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libsuitesparseconfig.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcholmod.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcolamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libccolamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libsuitesparseconfig.a
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libCGAL.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_thread.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_system.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libgmp.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libmpfr.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /usr/lib/libz.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcppunit.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libmpi_cxx.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libmpi.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libopen-rte.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libopen-pal.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /usr/lib/libutil.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkCommon.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkFiltering.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkImaging.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkGraphics.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkGenericFiltering.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkIO.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkRendering.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkVolumeRendering.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkHybrid.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkWidgets.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkInfovis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkGeovis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkViews.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkCharts.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_filesystem.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_program_options.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_iostreams.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_mpi.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_serialization.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_timer.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libboost_chrono.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/src/petsc/tferma-darwin11.1.0-cxx-opt/lib/libpetsc.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libumfpack.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcholmod.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcolamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libccolamd.a
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libsuitesparseconfig.a
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libparmetis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libmetis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libCGAL.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libgmp.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libmpfr.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /usr/lib/libz.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/libcppunit.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libmpi_cxx.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libmpi.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libopen-rte.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /Users/Asna/Work/TerraFERMA/lib/libopen-pal.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /usr/lib/libutil.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkCommon.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkFiltering.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkImaging.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkGraphics.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkGenericFiltering.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkIO.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkRendering.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkVolumeRendering.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkHybrid.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkWidgets.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkInfovis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkGeovis.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkViews.dylib
buckettools_ufc/libbuckettools_ufc.dylib: /opt/local/lib/vtk-5.10/libvtkCharts.dylib
buckettools_ufc/libbuckettools_ufc.dylib: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libbuckettools_ufc.dylib"
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/buckettools_ufc.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/build: buckettools_ufc/libbuckettools_ufc.dylib
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/build

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/requires: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemFunctionalsWrapper.cpp.o.requires
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/requires: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemSolversWrapper.cpp.o.requires
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/requires: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/SystemExpressionsWrapper.cpp.o.requires
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/requires: buckettools_ufc/CMakeFiles/buckettools_ufc.dir/VisualizationWrapper.cpp.o.requires
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/requires

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/clean:
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc && $(CMAKE_COMMAND) -P CMakeFiles/buckettools_ufc.dir/cmake_clean.cmake
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/clean

buckettools_ufc/CMakeFiles/buckettools_ufc.dir/depend: buckettools_ufc/SystemFunctionalsWrapper.cpp
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/depend: buckettools_ufc/SystemSolversWrapper.cpp
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/depend: buckettools_ufc/SystemExpressionsWrapper.cpp
buckettools_ufc/CMakeFiles/buckettools_ufc.dir/depend: buckettools_ufc/VisualizationWrapper.cpp
	cd /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/Asna/Work/TerraFERMA/share/terraferma/cpp /Users/Asna/Work/TerraFERMA/share/buckettools/ufc /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc /Users/Asna/apma4301_2013src/ProblemSet04/TerraFERMA/tutorials/poisson/mms/build_convergence/buckettools_ufc/CMakeFiles/buckettools_ufc.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : buckettools_ufc/CMakeFiles/buckettools_ufc.dir/depend

